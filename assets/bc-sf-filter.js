// Override Settings
var bcSfFilterSettings = {
  general: {
    limit: bcSfFilterConfig.custom.products_per_page,
    // Optional
    loadProductFirst: true,
    activeFilterScrollbar: "browser",
    paginationType: "load_more",
    activeScrollToTop: false,
  },
  label: {
    loadMore: "Load More Products",
  },
};

// Declare Templates
var bcSfFilterTemplate = {
  loadMoreButton: '<div id="bc-sf-filter-load-more">{{label.loadMore}}</div>',
  // Grid Template
  productGridItemHtml:
    '<div class="product-root" {{dataHoverImage}}>' +
    "{{itemLabels}}" +
    '<a href="{{itemUrl}}" aria-label="{{itemTitle}}" class="product-image-wrapper">' +
    "{{itemImage}}" +
    "{{itemHoverImage}}" +
    "</a>" +
    "{{reviewBlock}}" +
    '<div class="product--details">' +
    '<div class="custom-product-details">' +
    '<a href="{{itemUrl}}">' +
    '<div class="product--title font--block-heading">{{itemTitle}}</div>' +
    "</a>" +
    "{{itemVendor}}" +
    "</div>" +
    '<div class="product-price-container">' +
    '<a href="{{itemUrl}}">' +
    '<div class="product--price-wrapper">' +
    '<span class="product--price font--accent"><span class="money">' +
    "{{itemPrice}}" +
    "</span></span>" +
    "</div>" +
    "</a>" +
    "</div>" +
    '<div class="custom-iwish wishlist">{{itemWishlist}}</div>' +
    "{{addToCartBlock}}" +
    "</div>" +
    "</div>",
  // Pagination Template
  previousActiveHtml:
    '<li class="arrow left"><a href="{{itemUrl}}"><span class="glyph arrow-left" aria-hidden="true"></span> ' +
    bcSfFilterConfig.label.pagination_previous +
    "</a></li>",
  previousDisabledHtml:
    '<li class="arrow left unavailable"><a href=""><span class="glyph arrow-left" aria-hidden="true"></span> ' +
    bcSfFilterConfig.label.pagination_previous +
    "</a></li>",
  nextActiveHtml:
    '<li class="arrow right"><a href="{{itemUrl}}">' +
    bcSfFilterConfig.label.pagination_next +
    ' <span class="glyph arrow-right" aria-hidden="true"></span></a></li>',
  nextDisabledHtml:
    '<li class="arrow right unavailable"><a href="">' +
    bcSfFilterConfig.label.pagination_next +
    ' <span class="glyph arrow-right" aria-hidden="true"></span></a></li>',
  pageItemHtml: '<li><a href="{{itemUrl}}">{{itemTitle}}</a></li>',
  pageItemSelectedHtml: '<li class="current"><a href="">{{itemTitle}}</a></li>',
  pageItemRemainHtml:
    '<li class="unavailable"><a href="">{{itemTitle}}</a></li>',
  paginateHtml: "{{previous}}{{pageItems}}{{next}}",

  // Sorting Template
  sortingHtml:
    '<label for="bc-sf-filter-top-sorting-select">' +
    bcSfFilterConfig.label.sorting +
    '</label><select id="bc-sf-filter-top-sorting-select">{{sortingItems}}</select>',
};

// Build Product Grid Item
BCSfFilter.prototype.buildProductGridItem = function (data) {
  /*** Prepare data ***/
  var images = data.images_info;
  // Displaying price base on the policy of Shopify, have to multiple by 100
  var soldOut = !data.available; // Check a product is out of stock
  var onSale = data.compare_at_price_min > data.price_min; // Check a product is on sale
  var priceVaries = data.price_min != data.price_max; // Check a product has many prices
  // Get First Variant (selected_or_first_available_variant)
  var firstVariant = data["variants"][0];
  if (getParam("variant") !== null && getParam("variant") != "") {
    var paramVariant = data.variants.filter(function (e) {
      return e.id == getParam("variant");
    });
    if (typeof paramVariant[0] !== "undefined") firstVariant = paramVariant[0];
  } else {
    for (var i = 0; i < data["variants"].length; i++) {
      if (data["variants"][i].available) {
        firstVariant = data["variants"][i];
        break;
      }
    }
  }
  /*** End Prepare data ***/

  // Get Template
  var itemHtml = bcSfFilterTemplate.productGridItemHtml;

  // Add data hover image

  var dataHoverImage = bcSfFilterConfig.custom.hover_image_enable
    ? 'data-hover-image="loaded"'
    : "";
  if (images.length > 1) {
    itemHtml = itemHtml.replace(/{{dataHoverImage}}/g, dataHoverImage);
  } else {
    itemHtml = itemHtml.replace(/{{dataHoverImage}}/g, "");
  }

  // Add Labels
  var itemLabelsHtml = "";
  if (soldOut) {
    itemLabelsHtml += '<a href="{{itemUrl}}">';
    itemLabelsHtml += '<div class="product--sold-out"><span>';
    itemLabelsHtml += bcSfFilterConfig.label.sold_out;
    itemLabelsHtml += "</span></div>";
    itemLabelsHtml += "</a>";
  } else if (onSale) {
    itemLabelsHtml += '<a href="{{itemUrl}}">';
    itemLabelsHtml += '<div class="product--sale" href=""><span>';
    itemLabelsHtml += bcSfFilterConfig.label.sale;
    itemLabelsHtml += "</span></div>";
    itemLabelsHtml += "</a>";
  }
  itemHtml = itemHtml.replace(/{{itemLabels}}/g, itemLabelsHtml);

  // Add Image
  itemHtml = itemHtml.replace(/{{itemImage}}/g, buildItemImage(data, "image"));

  // Add Vendor
  var itemVendorHtml = "";
  if (bcSfFilterConfig.custom.vendor_enable) {
    itemVendorHtml +=
      '<p class="vendor"><a href="' +
      this.buildProductItemVendorUrl(data.vendor) +
      '">' +
      data.vendor +
      "</a>";
  }
  itemHtml = itemHtml.replace(/{{itemVendor}}/g, itemVendorHtml);

  // Add Price
  var itemPriceHtml = "";
  if (onSale) {
    itemPriceHtml +=
      '<span class="product--compare-price">' +
      '<span class="money">' +
      this.formatMoney(data.compare_at_price_min) +
      "</span>" +
      "</span>" +
      '<span class="product--price">' +
      '<span class="money">' +
      this.formatMoney(data.price_min) +
      "</span>" +
      "</span>";
  } else {
    if (priceVaries) {
      itemPriceHtml +=
        '<span class="product--from">' +
        bcSfFilterConfig.label.from_price +
        "</span>";
    }
    itemPriceHtml +=
      '<span class="product--price"><span class="money">' +
      this.formatMoney(data.price_min) +
      "</span></span>";
  }
  itemHtml = itemHtml.replace(/{{itemPrice}}/g, itemPriceHtml);

  // Add Hover Image
  // console.log(images.length);
  if (images.length > 1) {
    var itemHoverImage = bcSfFilterConfig.custom.hover_image_enable
      ? buildItemImage(data, "hover")
      : "";
    itemHtml = itemHtml.replace(/{{itemHoverImage}}/g, itemHoverImage);
  } else {
    itemHtml = itemHtml.replace(/{{itemHoverImage}}/g, "");
  }

  // Add wish list
  var itemWishListHtml =
    '<a class="iWishAddColl iwishcheck" data-variant="' +
    firstVariant.id +
    '" data-product="{{itemId}}" data-ptitle="{{itemTitle}}"><img src="https://cdn.shopify.com/s/files/1/0052/0041/2758/files/heart.svg?v=1583499456"></a>';
  itemHtml = itemHtml.replace(/{{itemWishlist}}/g, itemWishListHtml);

  // Review data
  var reviewBlockHtml =
    '<div class="card__review" data-bv-product-Id="{{itemId}}" data-bv-redirect-url="{{itemUrl}}" data-bv-show="inline_rating" ></div>';
  itemHtml = itemHtml.replace(/{{reviewBlock}}/g, reviewBlockHtml);

  // Add to cart form

  var addToCartHtml = "";
  if (!soldOut) {
    addToCartHtml +=
      '<div class="custom-btn-add" data-js-class="QACEProductAdd">' +
      '<form class="product-form" action="/cart/add" method="post">' +
      '<input type="hidden" name="id" value="' +
      firstVariant.id +
      '" />' +
      '<div class="add-to-cart">' +
      '<button class="btn" type="submit" name="add">' +
      '<span class="text">' +
      bcSfFilterConfig.label.addtocart +
      "</span>" +
      '<span class="fw--loading_animation tiny" data-js-class="FrameworkLoadingAnimation"></span>' +
      "</button></div></form></div>";
  }
  itemHtml = itemHtml.replace(/{{addToCartBlock}}/g, addToCartHtml);

  // Add main attribute
  itemHtml = itemHtml.replace(/{{itemId}}/g, data.id);
  itemHtml = itemHtml.replace(/{{itemHandle}}/g, data.handle);
  itemHtml = itemHtml.replace(/{{itemTitle}}/g, data.title);
  itemHtml = itemHtml.replace(/{{itemUrl}}/g, this.buildProductItemUrl(data));

  return itemHtml;
};

// Build Pagination
BCSfFilter.prototype.buildPagination = function (totalProduct) {
  // Get page info
  var currentPage = parseInt(this.queryParams.page);
  var totalPage = Math.ceil(totalProduct / this.queryParams.limit);

  // If it has only one page, clear Pagination
  if (totalPage == 1) {
    jQ(this.selector.pagination).html("");
    return false;
  }

  if (this.getSettingValue("general.paginationType") == "default") {
    var paginationHtml = bcSfFilterTemplate.paginateHtml;

    // Build Previous
    var previousHtml =
      currentPage > 1
        ? bcSfFilterTemplate.previousActiveHtml
        : bcSfFilterTemplate.previousDisabledHtml;
    previousHtml = previousHtml.replace(
      /{{itemUrl}}/g,
      this.buildToolbarLink("page", currentPage, currentPage - 1)
    );
    paginationHtml = paginationHtml.replace(/{{previous}}/g, previousHtml);

    // Build Next
    var nextHtml =
      currentPage < totalPage
        ? bcSfFilterTemplate.nextActiveHtml
        : bcSfFilterTemplate.nextDisabledHtml;
    nextHtml = nextHtml.replace(
      /{{itemUrl}}/g,
      this.buildToolbarLink("page", currentPage, currentPage + 1)
    );
    paginationHtml = paginationHtml.replace(/{{next}}/g, nextHtml);

    // Create page items array
    var beforeCurrentPageArr = [];
    for (
      var iBefore = currentPage - 1;
      iBefore > currentPage - 3 && iBefore > 0;
      iBefore--
    ) {
      beforeCurrentPageArr.unshift(iBefore);
    }
    if (currentPage - 4 > 0) {
      beforeCurrentPageArr.unshift("...");
    }
    if (currentPage - 4 >= 0) {
      beforeCurrentPageArr.unshift(1);
    }
    beforeCurrentPageArr.push(currentPage);

    var afterCurrentPageArr = [];
    for (
      var iAfter = currentPage + 1;
      iAfter < currentPage + 3 && iAfter <= totalPage;
      iAfter++
    ) {
      afterCurrentPageArr.push(iAfter);
    }
    if (currentPage + 3 < totalPage) {
      afterCurrentPageArr.push("...");
    }
    if (currentPage + 3 <= totalPage) {
      afterCurrentPageArr.push(totalPage);
    }

    // Build page items
    var pageItemsHtml = "";
    var pageArr = beforeCurrentPageArr.concat(afterCurrentPageArr);
    for (var iPage in pageArr) {
      if (pageArr[iPage] == "...") {
        pageItemsHtml += bcSfFilterTemplate.pageItemRemainHtml;
      } else {
        pageItemsHtml +=
          pageArr[iPage] == currentPage
            ? bcSfFilterTemplate.pageItemSelectedHtml
            : bcSfFilterTemplate.pageItemHtml;
      }
      pageItemsHtml = pageItemsHtml.replace(/{{itemTitle}}/g, pageArr[iPage]);
      pageItemsHtml = pageItemsHtml.replace(
        /{{itemUrl}}/g,
        this.buildToolbarLink("page", currentPage, pageArr[iPage])
      );
    }
    paginationHtml = paginationHtml.replace(/{{pageItems}}/g, pageItemsHtml);

    jQ(this.selector.pagination).html(paginationHtml);
  }
};

// Build Sorting
BCSfFilter.prototype.buildFilterSorting = function () {
  if (bcSfFilterTemplate.hasOwnProperty("sortingHtml")) {
    jQ(this.selector.topSorting).html("");

    var sortingArr = this.getSortingList();
    if (sortingArr) {
      // Build content
      var sortingItemsHtml = "";
      for (var k in sortingArr) {
        sortingItemsHtml +=
          '<option value="' + k + '">' + sortingArr[k] + "</option>";
      }
      var html = bcSfFilterTemplate.sortingHtml.replace(
        /{{sortingItems}}/g,
        sortingItemsHtml
      );
      jQ(this.selector.topSorting).html(html);

      // Set current value
      jQ(this.selector.topSorting + " select").val(this.queryParams.sort);
    }
  }
};

// Build srcset
// @data = data.images_info
function getSrcsetUrl(data) {
  var arr = [];
  var listImageWidth = getListImageWidth(data);
  var len = listImageWidth.length;
  var url = "";

  if (data != null) {
    for (var i = 0; i < len; i++) {
      url = data.src.substring(0, data.src.lastIndexOf("."));
      url += "_" + listImageWidth[i] + "x";
      url += data.src.substring(data.src.lastIndexOf("."));
      url += " " + listImageWidth[i] + "w";
      arr.push(url);
    }
  }

  return arr;
}

// Get Aspect Ratio based on Dimensions
// @data = data.images_info
function getAspectRatio(data) {
  var width = data == null ? "600" : data.width;
  var height = data == null ? "400" : data.height;

  return width / height;
}

// Get Image Width List
// @data = data.images_info
function getListImageWidth(data) {
  var arr = [295, 394, 590, 700, 800, 1000, 1200, 1500, 1800, 2000, 2400];
  var arr_len = arr.length;
  var rs = [];

  if (data != null) {
    for (var i = 0; i < arr_len; i++) {
      var width_init = arr[i] + 0;

      if (data.width > width_init) {
        rs.push(width_init);
      }
    }
    rs.push(data.width);
  }

  return rs;
}

function buildItemImage(data, type) {
  var itemImage = "";
  var index = Object.keys(data.images);
  var imgWidth =
    data.images_info[0] == null ? "600" : data.images_info[0].width;

  // Calculate padding-top based on aspect ratio
  var pad_top = (1 / getAspectRatio(data.images_info[0])) * 100;
  //console.log(index);
  // Image Section

  itemImage += '<div class="image--container">';

  // Get image src (type='image') & hover image src
  if (type === "image") {
    if (index.length > 0) {
      itemImage +=
        '<img class="lazyload" data-sizes="auto" data-widths="[' +
        getListImageWidth(data.images_info[0]) +
        ']" data-aspectratio="' +
        getAspectRatio(data.images_info[0]) +
        '" data-src="' +
        getSrcsetUrl(data.images_info[0]) +
        '" srcset="' +
        getSrcsetUrl(data.images_info[0]) +
        '" alt="' +
        data.title +
        '">';
    } else {
      itemImage +=
        '<img class="lazyload" data-sizes="auto" data-widths="[' +
        getListImageWidth(data.images_info[0]) +
        ']" src="' +
        bcSfFilterConfig.general.no_image_url +
        '" data-src="' +
        getSrcsetUrl(data.images_info[0]) +
        '" srcset="' +
        getSrcsetUrl(data.images_info[0]) +
        '" alt="' +
        data.title +
        '">';
    }
  } else {
    if (index.length > 0) {
      itemImage +=
        '<img class="lazyload lazypreload" data-sizes="auto" data-widths="[' +
        getListImageWidth(data.images_info[0]) +
        ']" data-aspectratio="' +
        getAspectRatio(data.images_info[0]) +
        '" data-src="' +
        getSrcsetUrl(data.images_info[data.images_info.length - 1]) +
        '" srcset="' +
        getSrcsetUrl(data.images_info[data.images_info.length - 1]) +
        '" alt="' +
        data.title +
        '">';
    } else {
      itemImage +=
        '<img class="lazyload lazypreload" data-sizes="auto" data-widths="[' +
        getListImageWidth(data.images_info[0]) +
        ']" src="' +
        bcSfFilterConfig.general.no_image_url +
        '" data-src="' +
        getSrcsetUrl(data.images_info[data.images_info.length - 1]) +
        '" srcset="' +
        getSrcsetUrl(data.images_info[data.images_info.length - 1]) +
        '" alt="' +
        data.title +
        '">';
    }
  }

  itemImage += "</div>";

  return itemImage;
}

// Build additional attributes of product items
BCSfFilter.prototype.buildExtrasProductList = function (data) {
  // jQ(".product--image-wrapper").each(function() {
  //     jQ(this).on('mouseover mouseout', function() {
  //         var item = jQ(this).children('.product--hover-image');
  //
  //         if (item.length > 0) {
  //             if (event.type == 'mouseover') {
  //                 jQ(item).attr('style', 'opacity: 1');
  //             } else {
  //                 jQ(item).attr('style', 'opacity: 0');
  //             }
  //         }
  //     });
  // });

  jQ("[data-js-class]").each(function () {
    var js_class, partial_class;
    js_class = $(this).attr("data-js-class");

    if (
      js_class === "QACEProductAdd" &&
      $(this).attr("data-js-loaded") !== "true"
    ) {
      //console.log("QA - Fired!");
      partial_class = theme.classes["CEProductAdd"];
      if (typeof partial_class !== "undefined") {
        theme.partials["CEProductAdd"] = new partial_class($(this));
        return $(this).attr("data-js-loaded", "true");
      }
    }
  });
};

// Build additional elements
BCSfFilter.prototype.buildAdditionalElements = function (data, eventType) {
  //console.log("buildAdditionalElements - " + eventType);

  // get number of products loaded and add to data object
  data.loaded_products = document.querySelectorAll("div.custom-btn-add").length;

  var _this = this;
  var results_label = bcSfFilterConfig.label.items_with_count_other;

  if (data.total_product == 1) {
    results_label = bcSfFilterConfig.label.items_with_count_one;
  }

  var totalProduct =
    "<span>" +
    bcSfFilterConfig.label.items_with_count_before +
    " </span>" +
    data.total_product +
    "<span> " +
    results_label +
    "</span>";

  jQ("#bc-sf-filter-total-product").html(totalProduct);

  jQ(".iWishAddColl.iwishcheck").on("click", function (e) {
    e.preventDefault();
    var iWishvId = jQ(this).attr("data-variant");
    iwish_addCollection(jQ(this), iWishvId);
    return false;
  });
  iwishCheckColl();
};

// Build Default layout
BCSfFilter.prototype.buildDefaultElements = function () {
  var isiOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream,
    isSafari = /Safari/.test(navigator.userAgent),
    isBackButton =
      window.performance &&
      window.performance.navigation &&
      2 == window.performance.navigation.type;
  if (!(isiOS && isSafari && isBackButton)) {
    var self = this,
      url = window.location.href.split("?")[0],
      searchQuery =
        self.isSearchPage() && self.queryParams.hasOwnProperty("q")
          ? "&q=" + self.queryParams.q
          : "";
    window.location.replace(url + "?view=bc-original" + searchQuery);
  }
};

function customizeJsonProductData(data) {
  for (var i = 0; i < data.variants.length; i++) {
    var variant = data.variants[i];
    var featureImage = data.images.filter(function (e) {
      return e.src == variant.image;
    });
    if (featureImage.length > 0) {
      variant.featured_image = {
        id: featureImage[0]["id"],
        product_id: data.id,
        position: featureImage[0]["position"],
        created_at: "",
        updated_at: "",
        alt: null,
        width: featureImage[0]["width"],
        height: featureImage[0]["height"],
        src: featureImage[0]["src"],
        variant_ids: [variant.id],
      };
    } else {
      variant.featured_image = "";
    }
  }
  var self = bcsffilter;
  var itemJson = {
    id: data.id,
    title: data.title,
    handle: data.handle,
    vendor: data.vendor,
    variants: data.variants,
    url: self.buildProductItemUrl(data),
    options_with_values: data.options_with_values,
    images: data.images,
    images_info: data.images_info,
    available: data.available,
    price_min: data.price_min,
    price_max: data.price_max,
    compare_at_price_min: data.compare_at_price_min,
    compare_at_price_max: data.compare_at_price_max,
  };
  return itemJson;
}
BCSfFilter.prototype.prepareProductData = function (data) {
  var countData = data.length;
  for (var k = 0; k < countData; k++) {
    data[k]["images"] = data[k]["images_info"];
    if (data[k]["images"].length > 0) {
      data[k]["featured_image"] = data[k]["images"][0];
    } else {
      data[k]["featured_image"] = {
        src: bcSfFilterConfig.general.no_image_url,
        width: "",
        height: "",
        aspect_ratio: 0,
      };
    }
    data[k]["url"] = "/products/" + data[k].handle;
    var optionsArr = [];
    var countOptionsWithValues = data[k]["options_with_values"].length;
    for (var i = 0; i < countOptionsWithValues; i++) {
      optionsArr.push(data[k]["options_with_values"][i]["name"]);
    }
    data[k]["options"] = optionsArr;
    if (
      typeof bcSfFilterConfig.general.currencies != "undefined" &&
      bcSfFilterConfig.general.currencies.length > 1
    ) {
      var currentCurrency = bcSfFilterConfig.general.current_currency
        .toLowerCase()
        .trim();
      function updateMultiCurrencyPrice(oldPrice, newPrice) {
        if (typeof newPrice != "undefined") {
          return newPrice;
        }
        return oldPrice;
      }
      data[k].price_min = updateMultiCurrencyPrice(
        data[k].price_min,
        data[k]["price_min_" + currentCurrency]
      );
      data[k].price_max = updateMultiCurrencyPrice(
        data[k].price_max,
        data[k]["price_max_" + currentCurrency]
      );
      data[k].compare_at_price_min = updateMultiCurrencyPrice(
        data[k].compare_at_price_min,
        data[k]["compare_at_price_min_" + currentCurrency]
      );
      data[k].compare_at_price_max = updateMultiCurrencyPrice(
        data[k].compare_at_price_max,
        data[k]["compare_at_price_max_" + currentCurrency]
      );
    }
    (data[k]["price_min"] *= 100),
      (data[k]["price_max"] *= 100),
      (data[k]["compare_at_price_min"] *= 100),
      (data[k]["compare_at_price_max"] *= 100);
    data[k]["price"] = data[k]["price_min"];
    data[k]["compare_at_price"] = data[k]["compare_at_price_min"];
    data[k]["price_varies"] = data[k]["price_min"] != data[k]["price_max"];
    var firstVariant = data[k]["variants"][0];
    if (getParam("variant") !== null && getParam("variant") != "") {
      var paramVariant = data[k]["variants"].filter(function (e) {
        return e.id == getParam("variant");
      });
      if (typeof paramVariant[0] !== "undefined")
        firstVariant = paramVariant[0];
    } else {
      var countVariants = data[k]["variants"].length;
      for (var i = 0; i < countVariants; i++) {
        if (data[k]["variants"][i].available) {
          firstVariant = data[k]["variants"][i];
          break;
        }
      }
    }
    data[k]["selected_or_first_available_variant"] = firstVariant;
    var countVariants = data[k]["variants"].length;
    for (var i = 0; i < countVariants; i++) {
      var variantOptionArr = [];
      var count = 1;
      var variant = data[k]["variants"][i];
      var variantOptions = variant["merged_options"];
      if (Array.isArray(variantOptions)) {
        var countVariantOptions = variantOptions.length;
        for (var j = 0; j < countVariantOptions; j++) {
          var temp = variantOptions[j].split(":");
          data[k]["variants"][i]["option" + (parseInt(j) + 1)] = temp[1];
          data[k]["variants"][i]["option_" + temp[0]] = temp[1];
          variantOptionArr.push(temp[1]);
        }
        data[k]["variants"][i]["options"] = variantOptionArr;
      }
      data[k]["variants"][i]["compare_at_price"] =
        parseFloat(data[k]["variants"][i]["compare_at_price"]) * 100;
      data[k]["variants"][i]["price"] =
        parseFloat(data[k]["variants"][i]["price"]) * 100;
    }
    data[k]["description"] = data[k]["content"] = data[k]["body_html"];
    if (
      data[k].hasOwnProperty("original_tags") &&
      data[k]["original_tags"].length > 0
    ) {
      data[k]["tags"] = data[k]["original_tags"].slice(0);
    }
    data[k]["json"] = customizeJsonProductData(data[k]);
  }
  return data;
};

BCSfFilter.prototype.getFilterData = function (eventType, errorCount) {
  function BCSend(eventType, errorCount) {
    var self = bcsffilter;
    var errorCount = typeof errorCount !== "undefined" ? errorCount : 0;
    self.showLoading();
    if (typeof self.buildPlaceholderProductList == "function") {
      self.buildPlaceholderProductList(eventType);
    }
    self.beforeGetFilterData(eventType);
    self.prepareRequestParams(eventType);
    self.queryParams["callback"] = "BCSfFilterCallback";
    self.queryParams["event_type"] = eventType;
    var url = self.isSearchPage()
      ? self.getApiUrl("search")
      : self.getApiUrl("filter");
    var script = document.createElement("script");
    script.type = "text/javascript";
    var timestamp = new Date().getTime();
    script.src = url + "?t=" + timestamp + "&" + jQ.param(self.queryParams);
    script.id = "bc-sf-filter-script";
    script.async = true;
    var resendAPITimer, resendAPIDuration;
    resendAPIDuration = 2e3;
    script.addEventListener("error", function (e) {
      if (typeof document.getElementById(script.id).remove == "function") {
        document.getElementById(script.id).remove();
      } else {
        document.getElementById(script.id).outerHTML = "";
      }
      if (errorCount < 3) {
        errorCount++;
        if (resendAPITimer) {
          clearTimeout(resendAPITimer);
        }
        resendAPITimer = setTimeout(
          self.getFilterData("resend", errorCount),
          resendAPIDuration
        );
      } else {
        self.buildDefaultElements(eventType);
      }
    });
    document.getElementsByTagName("head")[0].appendChild(script);
    script.onload = function () {
      if (typeof document.getElementById(script.id).remove == "function") {
        document.getElementById(script.id).remove();
      } else {
        document.getElementById(script.id).outerHTML = "";
      }
    };
  }
  this.requestFilter(BCSend, eventType, errorCount);
};
BCSfFilter.prototype.requestFilter = function (
  sendFunc,
  eventType,
  errorCount
) {
  sendFunc(eventType, errorCount);
};
