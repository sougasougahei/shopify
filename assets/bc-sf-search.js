// Override Settings
var bcSfSearchSettings = {
	search: {
		suggestionStyle: 'style2',
		suggestionStyle2MainContainerSelector: 'body',
		suggestionMobileStyle: 'style2',
	}
};

// Customize style of Suggestion box
BCSfFilter.prototype.customizeSuggestion = function (suggestionElement, searchElement, searchBoxId) {
};

BCSfFilter.prototype.initSearchBox = function (id) {
	if (this.getSettingValue('search.enableSuggestion')) {

		//Remove theme's instant search
		removeThemeSearchEvent();

		var self = this;
		if (typeof id === 'undefined') {
			jQ('input[name="' + this.searchTermKey + '"]').each(function (i) {
				if (!jQ(this)[0].hasAttribute('data-no-bc-search')) {
					var id = 'bc-sf-search-box-' + i;
					jQ(this).attr('id', id);
					self.buildSearchBox('#' + id)
				}
			});
		} else {
			this.buildSearchBox(id);
		}
		if (this.isMobile()) {
			// Clear cache when going back from another page
			window.onpageshow = function (event) {
				if (event.persisted) {
					window.location.reload();
				}
			};
			// Build Suggestion mobile on top
			if (this.getSettingValue('search.suggestionMobileStyle') == 'style1') {
				this.buildSuggestionMobile();
			}
		}
	}
};

function removeThemeSearchEvent() {
	// Remove all events
	if (jQ('[name="q"]').length > 0) {
		var cloneSearchBar = jQ('[name="q"]:first').clone();
        jQ('[name="q"]').replaceWith(cloneSearchBar);
        jQ('[name="q"]').wrap('<form action="/search" method="get" class="search-bar small--hide" role="search"></form>');
        
        // Fix style
        jQ('.search--results').attr("style", "display: none !important");
        jQ('[data-js-class="FrameworkSearch"]').attr("style", "background-color: white;");
        jQ('[data-js-class="FrameworkSearch"]').parent().attr("style", "background: rgba(0,0,0,0.1);");
	}
}