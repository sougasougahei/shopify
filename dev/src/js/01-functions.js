var Globals = (function ($) {
  function init() {
    megaNav();
    openCart();
    closeCart();
    openMenu();
    closeMenu();
    scroll();
    activeNav();
    accordion();
  }

  function megaNav() {
    $(".header-root nav span").hover(
      function (e) {
        $(this).addClass("active").children(".mega-nav").addClass("navActive");
        e.preventDefault();
      },
      function (e) {
        $(this)
          .removeClass("active")
          .children(".mega-nav")
          .removeClass("navActive");
        e.preventDefault();
      }
    );
  }

  function openCart() {
    $(".cart--open-right").on("click", function (e) {
      $(".off-canvas--viewport").attr("data-off-canvas--state", "right-open");
      $(".off-canvas--right-sidebar").addClass("open-state");
      $("body").addClass("overlay-fix");
      e.preventDefault();
    });
  }

  function closeCart() {
    $(
      ".off-canvas--close, .off-canvas--overlay, .cart--continue-shopping a"
    ).on("click", function (e) {
      $(".off-canvas--viewport").attr("data-off-canvas--state", "closed");
      $(".off-canvas--right-sidebar").removeClass("open-state");
      $("body").removeClass("overlay-fix");
      e.preventDefault();
    });
  }

  function openMenu() {
    $(".menu--open-left").on("click", function (e) {
      $(".off-canvas--viewport").attr("data-off-canvas--state", "left-open");
      $(".off-canvas--left-sidebar").addClass("open-state");
      $("body").addClass("overlay-fix");
      e.preventDefault();
    });
  }

  function closeMenu() {
    $(".off-canvas--close, .off-canvas--overlay").on("click", function (e) {
      $(".off-canvas--viewport").attr("data-off-canvas--state", "closed");
      $(".off-canvas--left-sidebar").removeClass("open-state");
      $("body").removeClass("overlay-fix");
      e.preventDefault();
    });
  }

  function scroll() {
    $(window).scroll(function () {
      if ($(this).scrollTop() > 700) {
        $(".sticky-header").addClass("stuck-header");
      } else {
        $(".sticky-header").removeClass("stuck-header");
      }
    });
  }

  function activeNav() {
    $(".header-root nav a").each(function (link) {
      if (link.href == document.location.href) {
        Element.addClassName(link, "active");
      }
    });
  }

  function accordion() {
    var allPanels = $(".accordion > dd").hide();
    $(".accordion > dt:first-child + dd").show().addClass("active");
    var thisPanel = $(".accordion > dt").removeClass("active");
    $(".accordion > dt:first-child").addClass("active");

    $(".accordion > dt > a").click(function () {
      $this = $(this);
      $target = $this.parent().next();
      $parent = $this.parent();

      if (!$parent.hasClass("active")) {
        thisPanel.removeClass("active");
        $parent.addClass("active");
      }

      if (!$target.hasClass("active")) {
        allPanels.removeClass("active").slideUp();
        $target.addClass("active").slideDown();
      }

      return false;
    });
  }

  return {
    init: init,
  };
})(jQuery);

jQuery(function () {
  Globals.init();
});
