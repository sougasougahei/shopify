(function () {
  var Accordion,
    Collection,
    DOC,
    Footer,
    FrameworkAlign,
    FullscreenSlider,
    HEADER,
    HorizontalTabs,
    MediaQueries,
    PAGE,
    Popup,
    ProductSlider,
    VerticalTabs,
    VideoModal,
    WINDOW,
    current_window,
    log,
    mq_large,
    mq_medium,
    mq_small,
    touchevents_exist,
    bind = function (fn, me) {
      return function () {
        return fn.apply(me, arguments);
      };
    },
    extend = function (child, parent) {
      for (var key in parent) {
        if (hasProp.call(parent, key)) child[key] = parent[key];
      }
      function ctor() {
        this.constructor = child;
      }
      ctor.prototype = parent.prototype;
      child.prototype = new ctor();
      child.__super__ = parent.prototype;
      return child;
    },
    hasProp = {}.hasOwnProperty;

  FrameworkAlign = (function () {
    function FrameworkAlign(element1, type1) {
      this.element = element1;
      this.type = type1;
      this.resizeListeners = bind(this.resizeListeners, this);
      this.fillYSpace = bind(this.fillYSpace, this);
      this.checkOverlap = bind(this.checkOverlap, this);
      this.createOverlapDetectors = bind(this.createOverlapDetectors, this);
      this.position = bind(this.position, this);
      this.parent = this.element.parent();
      this.left_side_border;
      this.right_element_width = null;
      this.position();
      this.fillYSpace();
      this.createOverlapDetectors();
      this.resizeListeners();
    }

    FrameworkAlign.prototype.position = function () {
      if (this.type === "center-x") {
        return this.element.css({
          "margin-left": -(this.element.outerWidth() / 2),
        });
      }
    };

    FrameworkAlign.prototype.createOverlapDetectors = function () {
      var _this;
      _this = this;
      if (_this.type === "right") {
        return (this.right_element_width = _this.element.outerWidth());
      }
    };

    FrameworkAlign.prototype.checkOverlap = function () {
      var _this, left_element, left_side_border, right_side_border;
      _this = this;
      if (_this.type === "right") {
        _this.parent.removeClass("overlap");
        left_element = _this.parent.children().eq(_this.element.index() - 1);
        left_side_border =
          left_element.position().left + left_element.outerWidth();
        right_side_border =
          _this.parent.outerWidth() - _this.right_element_width;
        if (left_side_border >= right_side_border) {
          return _this.parent.addClass("overlap");
        }
      }
    };

    FrameworkAlign.prototype.fillYSpace = function () {
      var _this, container_height, text_panel_height;
      _this = this;
      if (_this.type === "fill-y--dynamic") {
        container_height = _this.parent.outerHeight();
        text_panel_height = _this.parent
          .find(".fw--align--fill-y--fixed")
          .outerHeight();
        return _this.element.outerHeight(container_height - text_panel_height);
      }
    };

    FrameworkAlign.prototype.resizeListeners = function () {
      var _this;
      _this = this;
      $(window).on("resize.FrameworkAlign", function () {
        _this.checkOverlap();
        return _this.fillYSpace();
      });
      return setTimeout(function () {
        return _this.checkOverlap();
      }, 1);
    };

    return FrameworkAlign;
  })();

  theme.classes.FrameworkAnimation = (function () {
    function FrameworkAnimation() {}

    return FrameworkAnimation;
  })();

  theme.classes.FrameworkArticle = (function () {
    function FrameworkArticle(root) {
      var _this;
      this.root = root;
      this.startDisqus = bind(this.startDisqus, this);
      this.load = bind(this.load, this);
      _this = this;
      _this.load();
    }

    FrameworkArticle.prototype.load = function () {
      var _this, comments_enabled, enable_disqus;
      _this = this;
      enable_disqus =
        $(".article--root").attr("data-enable-disqus") === "true"
          ? true
          : false;
      comments_enabled =
        $(".article--root").attr("data-comments-enabled") === "true"
          ? true
          : false;
      if (enable_disqus && comments_enabled) {
        return _this.startDisqus();
      }
    };

    FrameworkArticle.prototype.startDisqus = function () {
      var _this, disqusConfig, disqus_shortname;
      _this = this;
      disqusConfig = function () {
        _this.page.url = $(".article--root").attr("data-canonical-url");
        return (_this.page.identifier = $(".article--root").attr(
          "data-article-id"
        ));
      };
      disqus_shortname = $(".article--root").attr("data-disqus-shortname");
      return (function () {
        var s;
        s = document.createElement("script");
        s.src = "https://" + disqus_shortname + ".disqus.com/embed.js";
        s.setAttribute("data-timestamp", +new Date());
        return (document.head || document.body).appendChild(s);
      })();
    };

    return FrameworkArticle;
  })();

  theme.classes.FrameworkBlog = (function () {
    function FrameworkBlog(root) {
      var _this;
      this.root = root;
      this.initMasonry = bind(this.initMasonry, this);
      this.eventListeners = bind(this.eventListeners, this);
      this.load = bind(this.load, this);
      _this = this;
      _this.load();
    }

    FrameworkBlog.prototype.load = function () {
      var _this, is_mobile;
      _this = this;
      _this.eventListeners();
      _this.has_multi_columns = _this.root.attr("data-columns") !== "1";
      is_mobile = theme.utils.mqs.current_window === "small";
      if (!is_mobile && _this.has_multi_columns) {
        _this.initMasonry();
        return $(window).on("load", function () {
          return _this.initMasonry();
        });
      }
    };

    FrameworkBlog.prototype.eventListeners = function () {
      var _this;
      _this = this;
      return $(window).on(
        "resize.Blog",
        theme.utils.debounce(100, function () {
          if (
            _this.has_multi_columns &&
            theme.utils.mqs.current_window !== "small"
          ) {
            return _this.initMasonry();
          } else {
            return $(".blog--list").masonry("destroy");
          }
        })
      );
    };

    FrameworkBlog.prototype.initMasonry = function () {
      return $(".blog--list").masonry({
        itemSelector: ".blog--list--item",
        percentPosition: true,
        horizontalOrder: true,
        columnWidth: ".blog--list--item",
      });
    };

    return FrameworkBlog;
  })();

  theme.classes.FrameworkCart = (function () {
    function FrameworkCart(root) {
      var _this;
      this.root = root;
      this.renderDynamicCheckoutButtons = bind(
        this.renderDynamicCheckoutButtons,
        this
      );
      this.updateCurrencies = bind(this.updateCurrencies, this);
      this.updateTotals = bind(this.updateTotals, this);
      this.updateAllHasItems = bind(this.updateAllHasItems, this);
      this.swapInImages = bind(this.swapInImages, this);
      this.updateHtml = bind(this.updateHtml, this);
      this.updateAllHtml = bind(this.updateAllHtml, this);
      this.getHtml = bind(this.getHtml, this);
      this.removeItem = bind(this.removeItem, this);
      this.updateQuantity = bind(this.updateQuantity, this);
      this.updateNote = bind(this.updateNote, this);
      this.addItem = bind(this.addItem, this);
      this.clearRequests = bind(this.clearRequests, this);
      this.htmlListener = bind(this.htmlListener, this);
      this.orderNotesListener = bind(this.orderNotesListener, this);
      this.removeButtonListener = bind(this.removeButtonListener, this);
      this.minusButtonListener = bind(this.minusButtonListener, this);
      this.plusButtonListener = bind(this.plusButtonListener, this);
      this.inputBoxListener = bind(this.inputBoxListener, this);
      this.eventListeners = bind(this.eventListeners, this);
      this.getAjaxUrl = bind(this.getAjaxUrl, this);
      this.getOtherCarts = bind(this.getOtherCarts, this);
      _this = this;
      _this.ajaxReq = {};
      _this.other_carts = _this.getOtherCarts();
      _this.total_item_count = $(".cart--external--total-items");
      _this.total_price = $(".cart--external--total-price");
      _this.updateTimer = {};
      _this.view = _this.root.data("view");
      _this.getAjaxUrl();
      _this.updateTotals();
      _this.htmlListener();
      _this.eventListeners();
      _this.renderDynamicCheckoutButtons();
    }

    FrameworkCart.prototype.getOtherCarts = function () {
      var _this, other_carts;
      _this = this;
      return (other_carts = $('[data-js-class="Cart"]').not(_this.root));
    };

    FrameworkCart.prototype.getAjaxUrl = function () {
      var _this;
      _this = this;
      _this.ajax_url = window.location.href;
      _this.ajax_url = _this.ajax_url.replace(
        window.location.hostname + window.location.pathname,
        window.location.hostname + "/cart"
      );
      _this.ajax_url = _this.ajax_url.replace("#", "");
      if (_this.ajax_url.indexOf("?") >= 0) {
        _this.ajax_url = _this.ajax_url + "&";
      } else {
        _this.ajax_url = _this.ajax_url + "?";
      }
      _this.ajax_desktop_url = _this.ajax_url + "view=ajax--desktop";
      return (_this.ajax_mobile_url = _this.ajax_url + "view=ajax--mobile");
    };

    FrameworkCart.prototype.eventListeners = function () {
      var _this;
      _this = this;
      _this.inputBoxListener();
      _this.plusButtonListener();
      _this.minusButtonListener();
      _this.removeButtonListener();
      return _this.orderNotesListener();
    };

    FrameworkCart.prototype.inputBoxListener = function () {
      var _this, input_box;
      _this = this;
      input_box = _this.root.find(".cart--quantity--input");
      input_box.on("keydown", function (event) {
        if (
          (event.which < 48 || event.which > 57) &&
          event.which !== 8 &&
          event.which !== 37 &&
          event.which !== 39 &&
          event.which !== 9
        ) {
          return event.preventDefault();
        }
      });
      return input_box.on("keyup", function (event) {
        var item_key, quantity;
        $(this).attr("data-loading", "true");
        item_key = $(this).closest(".cart--item").data("item-key");
        _this.clearRequests(item_key);
        if (event.which === 8 && $(this).val().length === 0) {
          return;
        }
        quantity = theme.utils.isNaN(parseInt($(this).val()))
          ? 1
          : parseInt($(this).val());
        if (quantity !== 0) {
          return _this.updateQuantity(item_key, quantity, 700, function (
            success
          ) {
            if (success) {
              return _this.updateAllHtml(function () {});
            }
          });
        } else {
          return _this.removeItem(item_key);
        }
      });
    };

    FrameworkCart.prototype.plusButtonListener = function () {
      var _this, plus_button;
      _this = this;
      plus_button = _this.root.find(".cart--item--quantity .plus");
      return plus_button.on("click", function () {
        var input, item_key, quantity;
        input = $(this).prev("input");
        quantity = theme.utils.isNaN(parseInt(input.val()))
          ? 1
          : parseInt(input.val()) + 1;
        input.val(quantity);
        input.attr("data-loading", "true");
        item_key = $(this).closest(".cart--item").data("item-key");
        _this.clearRequests(item_key);
        _this.updateQuantity(item_key, quantity, 700, function (success) {
          if (success) {
            return _this.updateAllHtml(function () {});
          }
        });
        return false;
      });
    };

    FrameworkCart.prototype.minusButtonListener = function () {
      var _this, minus_button;
      _this = this;
      minus_button = _this.root.find(".cart--item--quantity .minus");
      return minus_button.on("click", function () {
        var input, item_key, quantity;
        input = $(this).siblings("input");
        quantity = theme.utils.isNaN(parseInt(input.val()))
          ? 1
          : parseInt(input.val()) - 1;
        if (quantity < 1) {
          quantity = 1;
          return false;
        }
        input.val(quantity);
        input.attr("data-loading", "true");
        item_key = $(this).closest(".cart--item").data("item-key");
        _this.clearRequests(item_key);
        _this.updateQuantity(item_key, quantity, 700, function (success) {
          if (success) {
            return _this.updateAllHtml(function () {});
          }
        });
        return false;
      });
    };

    FrameworkCart.prototype.removeButtonListener = function () {
      var _this, remove_button;
      _this = this;
      remove_button = _this.root.find(".cart--item--remove");
      return remove_button.on("click", function () {
        var item, item_key;
        item = $(this).closest(".cart--item");
        item_key = item.data("item-key");
        item.find("input").attr("data-loading", "true");
        _this.clearRequests(item_key);
        _this.removeItem(item_key);
        return false;
      });
    };

    FrameworkCart.prototype.orderNotesListener = function () {
      var _this, note;
      _this = this;
      note = _this.root.find(".cart--notes--textarea");
      return note.on("keyup paste", function (event) {
        var order_note;
        clearTimeout(_this.typing_timer);
        order_note = $(this).val();
        return (_this.typing_timer = setTimeout(function () {
          clearTimeout(_this.typing_timer);
          return _this.updateNote(order_note);
        }, 700));
      });
    };

    FrameworkCart.prototype.htmlListener = function () {
      var _this;
      _this = this;
      return _this.root.on("updateHtml", function () {
        return _this.updateHtml(function () {});
      });
    };

    FrameworkCart.prototype.clearRequests = function (item_key) {
      var _this;
      _this = this;
      if (_this.ajaxReq.item_key) {
        _this.ajaxReq.item_key.abort();
      }
      if (_this.updateTimer.item_key) {
        return clearTimeout(_this.updateTimer.item_key);
      }
    };

    FrameworkCart.prototype.addItem = function (form, callback) {
      var _this;
      _this = this;
      return $.ajax({
        type: "POST",
        url: "/cart/add.js",
        dataType: "json",
        data: form.serialize(),
      })
        .done(function (data, textStatus, jqXHR) {
          callback(true);
          _this.updateAllHasItems(1, true);
          return _this.updateTotals();
        })
        .fail(function (jqXHR, textStatus) {
          console.log(
            "Error - ajax update quantity",
            JSON.parse(jqXHR.responseText).description
          );
          return callback(false, JSON.parse(jqXHR.responseText).description);
        });
    };

    FrameworkCart.prototype.updateNote = function (order_note) {
      var _this;
      _this = this;
      return $.ajax({
        type: "POST",
        url: "/cart/update.js",
        dataType: "json",
        data: {
          note: order_note,
        },
      })
        .done(function () {
          return _this.updateAllHtml(function () {});
        })
        .fail(function (jqXHR, textStatus) {
          return console.log(
            "Error - ajax update note",
            JSON.parse(jqXHR.responseText).description
          );
        });
    };

    FrameworkCart.prototype.updateQuantity = function (
      item_key,
      quantity,
      time_out,
      callback
    ) {
      var _this, ajaxCall;
      _this = this;
      ajaxCall = function () {
        return (_this.ajaxReq.item_key = $.ajax({
          type: "POST",
          url: "/cart/change.js",
          dataType: "json",
          data: "quantity=" + quantity + "&id=" + item_key,
        })
          .done(function (data, textStatus, jqXHR) {
            callback(true);
            return _this.updateTotals();
          })
          .fail(function (jqXHR, textStatus) {
            if (jqXHR.responseText) {
              console.log(
                "Error - ajax update quantity",
                JSON.parse(jqXHR.responseText).description
              );
            }
            return callback(false);
          }));
      };
      return (_this.updateTimer.item_key = setTimeout(ajaxCall, time_out));
    };

    FrameworkCart.prototype.removeItem = function (item_key) {
      var _this;
      _this = this;
      return _this.updateQuantity(item_key, 0, 0, function (success) {
        if (success) {
          return _this.updateAllHtml(function () {});
        }
      });
    };

    FrameworkCart.prototype.getHtml = function (view, callback) {
      var _this, url;
      _this = this;
      url = _this.ajax_desktop_url;
      if (view === "mobile") {
        url = _this.ajax_mobile_url;
      }
      return $.ajax({
        url: url,
        type: "GET",
        dataType: "html",
        success: function (data) {
          var cart_html;
          cart_html = $($.parseHTML(data));
          callback(cart_html);
        },
        error: function (jqxhr, textStatus, error) {
          var err;
          err = textStatus + ", " + error;
          console.log("search.json Request Failed: " + err);
          callback(false);
        },
      });
    };

    FrameworkCart.prototype.updateAllHtml = function (update_all_html_done) {
      var _this;
      _this = this;
      _this.updateHtml(update_all_html_done);
      return _this.other_carts.trigger("updateHtml");
    };

    FrameworkCart.prototype.updateHtml = function (update_all_html_done) {
      var _this;
      _this = this;
      return _this.getHtml(_this.view, function (new_html) {
        var new_form, old_form;
        old_form = _this.root.find(".cart--form");
        new_form = new_html.find(".cart--form");
        new_form = _this.swapInImages(old_form, new_form);
        old_form.replaceWith(new_form);
        _this.eventListeners();
        _this.updateCurrencies();
        return update_all_html_done();
      });
    };

    FrameworkCart.prototype.swapInImages = function (old_html, new_html) {
      var _this, new_items;
      _this = this;
      new_items = new_html.find(".cart--item");
      new_items.each(function () {
        var new_image, new_instance, old_image, old_item, variant_id;
        variant_id = $(this).data("variant-id");
        new_image = $(this).find(".cart--item--image");
        new_instance = new_html
          .find('[data-variant-id="' + variant_id + '"] .cart--item--image')
          .index(new_image);
        old_item = old_html
          .find('[data-variant-id="' + variant_id + '"]')
          .eq(new_instance);
        if (old_item.length > 0) {
          old_image = old_item.find(".cart--item--image");
          return new_image.replaceWith(old_image);
        }
      });
      return new_html;
    };

    FrameworkCart.prototype.updateAllHasItems = function (
      item_count,
      force_true
    ) {
      var _this, has_items;
      if (force_true == null) {
        force_true = false;
      }
      _this = this;
      has_items = false;
      if (item_count > 0 || force_true) {
        has_items = true;
      }
      _this.root.attr("data-has-items", has_items);
      return _this.other_carts.attr("data-has-items", has_items);
    };

    FrameworkCart.prototype.updateTotals = function () {
      var _this;
      _this = this;
      return $.ajax({
        type: "GET",
        url: "/cart.js",
        dataType: "json",
      })
        .done(function (data, textStatus, jqXHR) {
          var count, total_price;
          total_price = Shopify.formatMoney(
            data.total_price,
            theme.money_format
          );
          count = data.item_count;
          _this.updateAllHasItems(count);
          _this.total_price.text(total_price);
          _this.total_item_count.text(count);
          _this.updateTotalsComplete(count);
          return _this.updateCurrencies();
        })
        .fail(function (jqXHR, textStatus) {
          return console.log(
            "Error - ajax updating totals",
            JSON.parse(jqXHR.responseText).description
          );
        });
    };

    FrameworkCart.prototype.updateCurrencies = function () {
      if (theme.settings.currency_switcher_enabled) {
        return Currency.convertAll(shopCurrency, $("[name=currencies]").val());
      }
    };

    FrameworkCart.prototype.renderDynamicCheckoutButtons = function () {
      var _this;
      _this = this;
      if (window.location.pathname === "/cart") {
        $(".off-canvas--right-sidebar .cart--additional-buttons").remove();
        if (theme.utils.mqs.current_window === "small") {
          return $('[data-view="desktop"] .cart--additional-buttons').remove();
        }
      }
    };

    return FrameworkCart;
  })();

  theme.classes.FrameworkFeaturedBlog = (function () {
    function FrameworkFeaturedBlog(root) {
      var _this;
      this.root = root;
      this.resizeListeners = bind(this.resizeListeners, this);
      this.matchImageHeights = bind(this.matchImageHeights, this);
      _this = this;
      _this.item_container = _this.root.find(".featured-blog--body");
      _this.items = _this.root.find(".featured-blog--item");
      _this.matchImageHeights();
      _this.resizeListeners();
    }

    FrameworkFeaturedBlog.prototype.matchImageHeights = function () {
      var _this;
      _this = this;
      return theme.utils.matchImageHeights(
        _this.item_container,
        _this.items,
        ".featured-blog--item--image"
      );
    };

    FrameworkFeaturedBlog.prototype.resizeListeners = function () {
      var _this;
      _this = this;
      return $(window).on(
        "resize.FeaturedGrid",
        theme.utils.debounce(100, function () {
          return _this.matchImageHeights();
        })
      );
    };

    return FrameworkFeaturedBlog;
  })();

  theme.classes.FrameworkFeaturedCollections = (function () {
    function FrameworkFeaturedCollections(root) {
      var _this;
      this.root = root;
      this.resizeListeners = bind(this.resizeListeners, this);
      this.matchImageHeights = bind(this.matchImageHeights, this);
      _this = this;
      _this.item_container = _this.root.find(".featured-collections--body");
      _this.items = _this.root.find(".featured-collections--item");
      _this.matchImageHeights();
      _this.resizeListeners();
    }

    FrameworkFeaturedCollections.prototype.matchImageHeights = function () {
      var _this;
      _this = this;
      return theme.utils.matchImageHeights(
        _this.item_container,
        _this.items,
        ".featured-collections--image"
      );
    };

    FrameworkFeaturedCollections.prototype.resizeListeners = function () {
      var _this;
      _this = this;
      return $(window).on(
        "resize.FeaturedCollections",
        theme.utils.debounce(100, function () {
          return _this.matchImageHeights();
        })
      );
    };

    return FrameworkFeaturedCollections;
  })();

  theme.classes.FrameworkFeaturedContent = (function () {
    function FrameworkFeaturedContent(root) {
      var _this;
      this.root = root;
      _this = this;
    }

    return FrameworkFeaturedContent;
  })();

  theme.classes.FrameworkFeaturedGrid = (function () {
    function FrameworkFeaturedGrid(root) {
      var _this;
      this.root = root;
      this.resizeListeners = bind(this.resizeListeners, this);
      this.matchImageHeights = bind(this.matchImageHeights, this);
      _this = this;
      _this.item_container = _this.root.find(".featured-grid--body");
      _this.items = _this.root.find(".featured-grid--item");
      _this.text_position = _this.root.data("text-position");
      _this.matchImageHeights();
      _this.resizeListeners();
    }

    FrameworkFeaturedGrid.prototype.matchImageHeights = function () {
      var _this;
      _this = this;
      _this.items
        .find(".featured-grid--item--image, .placeholder--root")
        .css("height", "auto");
      if (
        _this.text_position === "center" &&
        theme.utils.mqs.current_window !== "small"
      ) {
        return;
      }
      return theme.utils.matchImageHeights(
        _this.item_container,
        _this.items,
        ".featured-grid--item--image"
      );
    };

    FrameworkFeaturedGrid.prototype.resizeListeners = function () {
      var _this;
      _this = this;
      return $(window).on(
        "resize.FeaturedGrid",
        theme.utils.debounce(100, function () {
          return _this.matchImageHeights();
        })
      );
    };

    return FrameworkFeaturedGrid;
  })();

  theme.classes.FrameworkFeaturedProduct = (function () {
    function FrameworkFeaturedProduct(root) {
      var _this;
      this.root = root;
      this.addProductComplete = bind(this.addProductComplete, this);
      this.addToCartListener = bind(this.addToCartListener, this);
      this.updateVariantImage = bind(this.updateVariantImage, this);
      this.sectionListeners = bind(this.sectionListeners, this);
      this.variantSelected = bind(this.variantSelected, this);
      this.initiateVariants = bind(this.initiateVariants, this);
      this.load = bind(this.load, this);
      _this = this;
      _this.id = _this.root.data("id");
      _this.add_button = _this.root.find(".featured-product--add-to-cart");
      _this.cart_form = _this.root.find(".featured-product--cart-form > form");
      _this.compare_price = _this.root.find(".featured-product--compare-price");
      _this.main_images = _this.root.find(".featured-product--image");
      _this.price = _this.root.find(".featured-product--actual-price");
      _this.price_wrapper = _this.root.find(".featured-product--price-wrapper");
      _this.smart_payment_buttons = _this.root.find(
        ".featured-product--smart-payment-buttons"
      );
      _this.unavailable_variant = _this.root.find(
        ".featured-product--unavailable-variant"
      );
      _this.variant_sold_out = _this.root.find(
        ".featured-product--sold-out--variant"
      );
      _this.cart_form = _this.root.find(".featured-product--cart-form > form");
      _this.json = JSON.parse(
        _this.root.find(".featured-product--json").text()
      );
      _this.load();
    }

    FrameworkFeaturedProduct.prototype.load = function () {
      var _this;
      _this = this;
      _this.initiateVariants();
      return _this.addToCartListener();
    };

    FrameworkFeaturedProduct.prototype.initiateVariants = function () {
      var _this, id, options;
      _this = this;
      if (_this.root.find("select").length > 0) {
        id = "featured-product--select--" + _this.id;
        return (options = new Shopify.OptionSelectors(id, {
          product: _this.json,
          onVariantSelected: _this.variantSelected,
          enableHistoryState: false,
        }));
      }
    };

    FrameworkFeaturedProduct.prototype.variantSelected = function (
      variant,
      selector,
      options
    ) {
      var _this, product_sold_out, variant_compare_price, variant_price;
      _this = this;
      product_sold_out = false;
      if (_this.root.find(".featured-product--sold-out--product").length) {
        product_sold_out = true;
      }
      _this.variant_sold_out.hide();
      _this.unavailable_variant.hide();
      _this.add_button.hide();
      _this.price_wrapper.hide();
      if (!variant) {
        _this.unavailable_variant.show();
        _this.smart_payment_buttons.hide();
        return false;
      } else if (variant && variant.available) {
        _this.price_wrapper.show();
        _this.add_button.show();
        _this.smart_payment_buttons.show();
      } else if (product_sold_out) {
        _this.variant_sold_out.hide();
        _this.add_button.hide();
        _this.smart_payment_buttons.hide();
      } else {
        _this.variant_sold_out.show();
        _this.smart_payment_buttons.hide();
      }
      variant_price = Shopify.formatMoney(variant.price, theme.money_format);
      _this.price.replaceWith(
        '<span class="featured-product--actual-price money font--accent">' +
          variant_price +
          "</span>"
      );
      _this.price = _this.root.find(".featured-product--actual-price");
      if (variant.compare_at_price > variant.price) {
        variant_compare_price = Shopify.formatMoney(
          variant.compare_at_price,
          theme.money_format
        );
        _this.compare_price.replaceWith(
          '<span class="featured-product--compare-price money font--light-accent">' +
            variant_compare_price +
            "</span>"
        );
        _this.compare_price = _this.root.find(
          ".featured-product--compare-price"
        );
        _this.compare_price.show();
      } else {
        _this.compare_price.hide();
      }
      if (theme.settings.currency_switcher_enabled) {
        Currency.convertAll(shopCurrency, $("[name=currencies]").val());
      }
      if (variant.featured_image) {
        return _this.updateVariantImage(variant.featured_image.id);
      }
    };

    FrameworkFeaturedProduct.prototype.sectionListeners = function () {
      var _this;
      _this = this;
      return _this.root.on("theme:section:load", function () {
        return new Shopify.OptionSelectors("featured-product--select", {
          product: theme.product_json,
          onVariantSelected: _this.variantSelected,
          enableHistoryState: true,
        });
      });
    };

    FrameworkFeaturedProduct.prototype.updateVariantImage = function (
      variant_id
    ) {
      var _this, variant_image;
      _this = this;
      _this.main_images.attr("data-active", "false");
      variant_image = _this.main_images.filter(
        '[data-image-id="' + variant_id + '"]'
      );
      return variant_image.attr("data-active", "true");
    };

    FrameworkFeaturedProduct.prototype.addToCartListener = function () {
      var _this;
      _this = this;
      if (_this.cart_form.length > 0 && theme.settings.cart_type === "drawer") {
        return _this.cart_form.on("submit", function () {
          _this.cart_form.find(".error").remove();
          _this.add_button.attr("data-loading", "true");
          theme.partials.Cart.addItem($(this), function (success, error) {
            if (success) {
              return theme.partials.Cart.updateAllHtml(function () {
                return _this.addProductComplete();
              });
            } else {
              _this.cart_form.append(
                '<div class="featured-product--error error">' + error + "</div>"
              );
              return _this.add_button.attr("data-loading", "false");
            }
          });
          return false;
        });
      }
    };

    FrameworkFeaturedProduct.prototype.addProductComplete = function () {
      var _this;
      _this = this;
      _this.add_button.attr("data-loading", "false");
      return $('[data-off-canvas--open="right-sidebar"]')
        .first()
        .trigger("click");
    };

    return FrameworkFeaturedProduct;
  })();

  theme.classes.FrameworkFeaturedVideo = (function () {
    function FrameworkFeaturedVideo(root) {
      var _this;
      this.root = root;
      this.hideThumbnail = bind(this.hideThumbnail, this);
      this.playButtonListener = bind(this.playButtonListener, this);
      this.vimeoEvents = bind(this.vimeoEvents, this);
      this.insertVimeoPlayer = bind(this.insertVimeoPlayer, this);
      this.youtubeEvents = bind(this.youtubeEvents, this);
      this.youtubeReady = bind(this.youtubeReady, this);
      this.insertYoutubePlayer = bind(this.insertYoutubePlayer, this);
      this.insertAPIScript = bind(this.insertAPIScript, this);
      this.playerInit = bind(this.playerInit, this);
      this.checkAPIScriptExists = bind(this.checkAPIScriptExists, this);
      _this = this;
      _this.video_type = _this.root.data("videoType");
      _this.video_id = _this.root.data("videoId");
      _this.thumbnail = _this.root.data("thumbnail");
      _this.section_id = _this.root.data("sectionId");
      _this.youtubeVars = {};
      _this.vimeoVars = {
        id: _this.video_id,
        autopause: 0,
        playsinline: 0,
        title: 0,
      };
      if (_this.thumbnail) {
        _this.playButtonListener();
      } else {
        _this.checkAPIScriptExists();
      }
    }

    FrameworkFeaturedVideo.prototype.checkAPIScriptExists = function () {
      var _this;
      _this = this;
      if (_this.video_type === "vimeo") {
        if (theme.utils.vimeoScriptAdded) {
          return _this.playerInit();
        } else {
          return _this.insertAPIScript(
            "https://player.vimeo.com/api/player.js"
          );
        }
      } else {
        if (theme.utils.youtubeScriptAdded) {
          return _this.playerInit();
        } else {
          return _this.insertAPIScript("https://www.youtube.com/iframe_api");
        }
      }
    };

    FrameworkFeaturedVideo.prototype.playerInit = function () {
      var _this;
      _this = this;
      if (_this.video_type === "vimeo") {
        if (_this.thumbnail) {
          return _this.insertVimeoPlayer();
        } else {
          $(window).on("load", function () {
            return _this.insertVimeoPlayer();
          });
          return _this.root.on("theme:section:load", function () {
            return _this.insertVimeoPlayer();
          });
        }
      } else {
        if (_this.thumbnail) {
          return _this.insertYoutubePlayer();
        } else {
          $(window).on("load", function () {
            return _this.insertYoutubePlayer();
          });
          return _this.root.on("theme:section:load", function () {
            return _this.insertYoutubePlayer();
          });
        }
      }
    };

    FrameworkFeaturedVideo.prototype.insertAPIScript = function (api_url) {
      var _this, script_tag;
      _this = this;
      script_tag = document.createElement("script");
      script_tag.src = api_url;
      if (_this.video_type === "vimeo") {
        theme.utils.vimeoScriptAdded = true;
        script_tag.onload = function () {
          return _this.insertVimeoPlayer();
        };
      } else {
        theme.utils.youtubeScriptAdded = true;
        $(window).on("theme.utils.youtubeAPIReady", function () {
          return _this.insertYoutubePlayer();
        });
      }
      return document.body.appendChild(script_tag);
    };

    FrameworkFeaturedVideo.prototype.insertYoutubePlayer = function () {
      var _this;
      _this = this;
      if (!_this.thumbnail) {
        _this.youtubeVars.playsinline = 1;
        _this.youtubeVars.fs = 0;
        _this.youtubeVars.loop = 1;
        _this.youtubeVars.playlist = _this.video_id;
      }
      return (_this.player = new YT.Player("player-" + _this.section_id, {
        videoId: _this.video_id,
        playerVars: _this.youtubeVars,
        events: {
          onReady: _this.youtubeReady,
          onStateChange: _this.youtubeEvents,
        },
      }));
    };

    FrameworkFeaturedVideo.prototype.youtubeReady = function () {
      var _this;
      _this = this;
      if (!_this.thumbnail) {
        _this.player.mute();
      }
      return _this.player.playVideo();
    };

    FrameworkFeaturedVideo.prototype.youtubeEvents = function (event) {
      var YTP, _this, remains;
      _this = this;
      YTP = event.target;
      if (_this.thumbnail) {
        if (event.data === 0) {
          YTP.seekTo(0);
          return YTP.pauseVideo();
        }
      } else {
        if (event.data === 1) {
          remains = YTP.getDuration() - YTP.getCurrentTime();
          if (_this.rewindTO) {
            clearTimeout(_this.rewindTO);
          }
          return (_this.rewindTO = setTimeout(function () {
            YTP.seekTo(0);
          }, (remains - 0.1) * 1000));
        }
      }
    };

    FrameworkFeaturedVideo.prototype.insertVimeoPlayer = function () {
      var _this;
      _this = this;
      if (!_this.thumbnail) {
        _this.vimeoVars.playsinline = 1;
        _this.vimeoVars.muted = 1;
        _this.vimeoVars.background = 1;
        _this.vimeoVars.loop = 1;
      }
      _this.player = new Vimeo.Player(
        "player-" + _this.section_id,
        _this.vimeoVars
      );
      if (_this.thumbnail) {
        _this.vimeoEvents();
      }
      return _this.player.play();
    };

    FrameworkFeaturedVideo.prototype.vimeoEvents = function () {
      var _this;
      _this = this;
      _this.player.getDuration().then(function (duration) {
        return _this.player.addCuePoint(duration - 0.3, {});
      });
      return _this.player.on("cuepoint", function () {
        _this.player.setCurrentTime(0);
        return _this.player.pause();
      });
    };

    FrameworkFeaturedVideo.prototype.playButtonListener = function () {
      var _this;
      _this = this;
      return _this.root
        .find(".feature-video--play svg, .feature-video--play-mobile svg")
        .on("click", function () {
          _this.checkAPIScriptExists();
          return _this.hideThumbnail();
        });
    };

    FrameworkFeaturedVideo.prototype.hideThumbnail = function () {
      var _this;
      _this = this;
      return setTimeout(function () {
        return _this.root
          .find(
            ".feature-video--header, .feature-video--thumbnail, .feature-video--play-mobile"
          )
          .hide();
      }, 350);
    };

    return FrameworkFeaturedVideo;
  })();

  theme.classes.FrameworkFeedbackBar = (function () {
    function FrameworkFeedbackBar(root) {
      var _this;
      this.root = root;
      this.load = bind(this.load, this);
      _this = this;
      _this.messages = _this.root.find(".feedback-bar--message span");
      _this.load();
    }

    FrameworkFeedbackBar.prototype.load = function () {
      var _this, anchor_tag, message, message_elem;
      _this = this;
      _this.messages.hide();
      anchor_tag = window.location.hash.substr(1);
      message = anchor_tag.replace("feedback-bar--", "");
      message_elem = _this.messages.filter(
        '[data-feedback-bar--message="' + message + '"]'
      );
      if (message_elem.length) {
        message_elem.show();
        setTimeout(function () {
          return _this.root.attr("data-feedback-bar--open", "true");
        }, 200);
        return setTimeout(function () {
          return _this.root.attr("data-feedback-bar--open", "false");
        }, 3000);
      }
    };

    return FrameworkFeedbackBar;
  })();

  theme.classes.FrameworkInstagram = (function () {
    function FrameworkInstagram(container1) {
      var _this;
      this.container = container1;
      this.load = bind(this.load, this);
      _this = this;
      _this.username = _this.container.data("username");
      _this.access_token = _this.container.data("access-token");
      _this.aria_label = _this.container.data("aria-label");
      _this.body = _this.container.find(".instagram--grid");
      _this.rows = _this.container.data("rows");
      _this.columns = _this.container.data("columns");
      _this.total_items = _this.rows * _this.columns;
      if (theme.utils.mqs.current_window === "small") {
        _this.total_items = 4;
      }
      _this.load();
    }

    FrameworkInstagram.prototype.load = function () {
      var _this;
      _this = this;
      if (_this.access_token.length < 1) {
        return false;
      }
      return $.ajax({
        dataType: "jsonp",
        url:
          "https://api.instagram.com/v1/users/self/media/recent/?access_token=" +
          _this.access_token +
          "&count=" +
          _this.total_items,
        success: function (response) {
          var data, i, img_src, j, ref, results1;
          if (response.meta.code === 200) {
            data = response.data;
            results1 = [];
            for (
              i = j = 0, ref = _this.total_items - 1;
              0 <= ref ? j <= ref : j >= ref;
              i = 0 <= ref ? ++j : --j
            ) {
              if (data[i]) {
                img_src = data[i].images.standard_resolution.url;
                img_src = img_src.replace("http:", "https:");
                results1.push(
                  _this.body.append(
                    "<a class='instagram--item' rel='noreferrer' target='_blank' aria-label='" +
                      _this.aria_label +
                      "' href='" +
                      data[i].link +
                      "' > <div class='instagram--item-container'> <img class='instagram--image lazyload' data-src='" +
                      img_src +
                      "' alt='" +
                      _this.aria_label +
                      "' > </div> </div> </a>"
                  )
                );
              } else {
                results1.push(void 0);
              }
            }
            return results1;
          } else {
            return _this.body.append(
              "<h4 class='error'>" + response.meta.error_message + "</h4>"
            );
          }
        },
        error: function (jqXHR, textStatus) {
          console.log(jqXHR);
          return console.log(textStatus);
        },
      });
    };

    return FrameworkInstagram;
  })();

  // theme.classes.FrameworkLoadingAnimation = (function () {
  //   function FrameworkLoadingAnimation(root) {
  //     var _this;
  //     this.root = root;
  //     _this = this;
  //     if (_this.root.hasClass("tiny")) {
  //       _this.root.spin("tiny");
  //     } else {
  //       _this.root.spin("small");
  //     }
  //   }
  //
  //   return FrameworkLoadingAnimation;
  // })();

  theme.classes.FrameworkMap = (function () {
    function FrameworkMap(root) {
      var _this;
      this.root = root;
      this.buildStyles = bind(this.buildStyles, this);
      this.buildMap = bind(this.buildMap, this);
      this.geolocate = bind(this.geolocate, this);
      this.loadMapsApi = bind(this.loadMapsApi, this);
      this.load = bind(this.load, this);
      _this = this;
      _this.api_status = null;
      _this.map_instance = null;
      _this.key = _this.root.data("api-key");
      _this.address = _this.root.data("address");
      _this.theme = _this.root.data("theme");
      _this.styles = null;
      _this.container = _this.root.find(".map--google-maps");
      _this.center = null;
      _this.load();
    }

    FrameworkMap.prototype.load = function () {
      var _this;
      _this = this;
      if (_this.container.length > 0) {
        return _this.loadMapsApi();
      }
    };

    FrameworkMap.prototype.loadMapsApi = function () {
      var _this, other_map_loading_checker;
      _this = this;
      if (theme.utils.google_map_api_status === "loading") {
        return (other_map_loading_checker = setInterval(function () {
          if (theme.utils.google_map_api_status === "loaded") {
            _this.geolocate();
            return clearInterval(other_map_loading_checker);
          }
        }, 100));
      } else if (typeof window.google === "undefined") {
        theme.utils.google_map_api_status = "loading";
        return $.getScript(
          "https://maps.googleapis.com/maps/api/js?key=" + _this.key
        ).then(function () {
          _this.geolocate();
          return (theme.utils.google_map_api_status = "loaded");
        });
      } else {
        return _this.geolocate();
      }
    };

    FrameworkMap.prototype.geolocate = function () {
      var _this, geocoder;
      _this = this;
      geocoder = new google.maps.Geocoder();
      return geocoder.geocode(
        {
          address: _this.address,
        },
        function (results, status) {
          if (status === "OK") {
            _this.center = results[0].geometry.location;
            _this.buildStyles();
            return _this.buildMap();
          } else {
            return console.log("couldn't convert address with geocoder");
          }
        }
      );
    };

    FrameworkMap.prototype.buildMap = function () {
      var _this, center, map, mapOptions, marker;
      _this = this;
      mapOptions = {
        zoom: 15,
        center: _this.center,
        disableDefaultUI: true,
        zoomControl: true,
        scrollwheel: false,
        styles: _this.styles,
      };
      map = new google.maps.Map(_this.container[0], mapOptions);
      center = map.getCenter();
      marker = new google.maps.Marker({
        map: map,
        position: map.getCenter(),
      });
      return (_this.map_instance = google.maps.event.addDomListener(
        window,
        "resize",
        theme.utils.debounce(500, function () {
          google.maps.event.trigger(map, "resize");
          map.setCenter(center);
        })
      ));
    };

    FrameworkMap.prototype.buildStyles = function () {
      var _this;
      _this = this;
      if (_this.theme === "grayscale") {
        return (_this.styles = [
          {
            elementType: "geometry",
            stylers: [
              {
                color: "#f5f5f5",
              },
            ],
          },
          {
            elementType: "labels.icon",
            stylers: [
              {
                visibility: "off",
              },
            ],
          },
          {
            elementType: "labels.text.fill",
            stylers: [
              {
                color: "#616161",
              },
            ],
          },
          {
            elementType: "labels.text.stroke",
            stylers: [
              {
                color: "#f5f5f5",
              },
            ],
          },
          {
            featureType: "administrative.land_parcel",
            elementType: "labels.text.fill",
            stylers: [
              {
                color: "#bdbdbd",
              },
            ],
          },
          {
            featureType: "poi",
            elementType: "geometry",
            stylers: [
              {
                color: "#eeeeee",
              },
            ],
          },
          {
            featureType: "poi",
            elementType: "labels.text.fill",
            stylers: [
              {
                color: "#757575",
              },
            ],
          },
          {
            featureType: "poi.park",
            elementType: "geometry",
            stylers: [
              {
                color: "#e5e5e5",
              },
            ],
          },
          {
            featureType: "poi.park",
            elementType: "labels.text.fill",
            stylers: [
              {
                color: "#9e9e9e",
              },
            ],
          },
          {
            featureType: "road",
            elementType: "geometry",
            stylers: [
              {
                color: "#ffffff",
              },
            ],
          },
          {
            featureType: "road.arterial",
            elementType: "labels.text.fill",
            stylers: [
              {
                color: "#757575",
              },
            ],
          },
          {
            featureType: "road.highway",
            elementType: "geometry",
            stylers: [
              {
                color: "#dadada",
              },
            ],
          },
          {
            featureType: "road.highway",
            elementType: "labels.text.fill",
            stylers: [
              {
                color: "#616161",
              },
            ],
          },
          {
            featureType: "road.local",
            elementType: "labels.text.fill",
            stylers: [
              {
                color: "#9e9e9e",
              },
            ],
          },
          {
            featureType: "transit.line",
            elementType: "geometry",
            stylers: [
              {
                color: "#e5e5e5",
              },
            ],
          },
          {
            featureType: "transit.station",
            elementType: "geometry",
            stylers: [
              {
                color: "#eeeeee",
              },
            ],
          },
          {
            featureType: "water",
            elementType: "geometry",
            stylers: [
              {
                color: "#c9c9c9",
              },
            ],
          },
          {
            featureType: "water",
            elementType: "labels.text.fill",
            stylers: [
              {
                color: "#9e9e9e",
              },
            ],
          },
        ]);
      } else if (_this.theme === "dark") {
        return (_this.styles = [
          {
            elementType: "geometry",
            stylers: [
              {
                color: "#212121",
              },
            ],
          },
          {
            elementType: "labels.icon",
            stylers: [
              {
                visibility: "off",
              },
            ],
          },
          {
            elementType: "labels.text.fill",
            stylers: [
              {
                color: "#757575",
              },
            ],
          },
          {
            elementType: "labels.text.stroke",
            stylers: [
              {
                color: "#212121",
              },
            ],
          },
          {
            featureType: "administrative",
            elementType: "geometry",
            stylers: [
              {
                color: "#757575",
              },
            ],
          },
          {
            featureType: "administrative.country",
            elementType: "labels.text.fill",
            stylers: [
              {
                color: "#9e9e9e",
              },
            ],
          },
          {
            featureType: "administrative.land_parcel",
            stylers: [
              {
                visibility: "off",
              },
            ],
          },
          {
            featureType: "administrative.locality",
            elementType: "labels.text.fill",
            stylers: [
              {
                color: "#bdbdbd",
              },
            ],
          },
          {
            featureType: "poi",
            elementType: "labels.text.fill",
            stylers: [
              {
                color: "#757575",
              },
            ],
          },
          {
            featureType: "poi.park",
            elementType: "geometry",
            stylers: [
              {
                color: "#181818",
              },
            ],
          },
          {
            featureType: "poi.park",
            elementType: "labels.text.fill",
            stylers: [
              {
                color: "#616161",
              },
            ],
          },
          {
            featureType: "poi.park",
            elementType: "labels.text.stroke",
            stylers: [
              {
                color: "#1b1b1b",
              },
            ],
          },
          {
            featureType: "road",
            elementType: "geometry.fill",
            stylers: [
              {
                color: "#2c2c2c",
              },
            ],
          },
          {
            featureType: "road",
            elementType: "labels.text.fill",
            stylers: [
              {
                color: "#8a8a8a",
              },
            ],
          },
          {
            featureType: "road.arterial",
            elementType: "geometry",
            stylers: [
              {
                color: "#373737",
              },
            ],
          },
          {
            featureType: "road.highway",
            elementType: "geometry",
            stylers: [
              {
                color: "#3c3c3c",
              },
            ],
          },
          {
            featureType: "road.highway.controlled_access",
            elementType: "geometry",
            stylers: [
              {
                color: "#4e4e4e",
              },
            ],
          },
          {
            featureType: "road.local",
            elementType: "labels.text.fill",
            stylers: [
              {
                color: "#616161",
              },
            ],
          },
          {
            featureType: "transit",
            elementType: "labels.text.fill",
            stylers: [
              {
                color: "#757575",
              },
            ],
          },
          {
            featureType: "water",
            elementType: "geometry",
            stylers: [
              {
                color: "#000000",
              },
            ],
          },
          {
            featureType: "water",
            elementType: "labels.text.fill",
            stylers: [
              {
                color: "#3d3d3d",
              },
            ],
          },
        ]);
      }
    };

    return FrameworkMap;
  })();

  theme.classes.FrameworkMediaQueries = (function () {
    function FrameworkMediaQueries() {
      this.screenSizeListener = bind(this.screenSizeListener, this);
      this.getScreenSize = bind(this.getScreenSize, this);
      this.medium_screen = 768;
      this.large_screen = 1024;
      this.current_window = null;
      this.getScreenSize();
      this.screenSizeListener();
    }

    FrameworkMediaQueries.prototype.getScreenSize = function () {
      var _this;
      _this = this;
      if (
        window.matchMedia(
          "only screen and (min-width: " + _this.large_screen + "px)"
        ).matches
      ) {
        if (_this.current_window !== "large") {
          return (_this.current_window = "large");
        }
      } else if (
        window.matchMedia(
          "only screen and (min-width: " + _this.medium_screen + "px)"
        ).matches
      ) {
        if (_this.current_window !== "medium") {
          return (_this.current_window = "medium");
        }
      } else {
        if (_this.current_window !== "small") {
          return (_this.current_window = "small");
        }
      }
    };

    FrameworkMediaQueries.prototype.screenSizeListener = function () {
      var _this;
      _this = this;
      return $(window).on("resize", function () {
        return _this.getScreenSize();
      });
    };

    return FrameworkMediaQueries;
  })();

  theme.classes.FrameworkModal = (function () {
    function FrameworkModal(container1) {
      var _this;
      this.container = container1;
      _this = this;
      _this.fullscreen = _this.container.data("modal--fullscreen")
        ? true
        : false;
      if (_this.container.data("modal--custom-close")) {
        _this.custom_close_button = _this.container.data("modal--custom-close");
      } else {
        _this.custom_close_button = "";
      }
      _this.force_view = _this.container.data("force-view");
      _this.view = _this.container.data("modal--view");
      _this.links = _this.container.find(".modal--link");
      _this.content = _this.container.find(".modal--content");
      _this.window = _this.createModalWindow();
      _this.window_container = _this.window.find(".modal--container");
      _this.mask = _this.window.find(".modal--mask");
      _this.close_button = _this.window.find(".modal--close");
      _this.next_button = _this.window.find(".modal--next");
      _this.prev_button = _this.window.find(".modal--prev");
      _this.slider = null;
      _this.slides = null;
      _this.main_content_window = $(".off-canvas--main-content");
      _this.openListeners();
      _this.modal_state = "closed";
      _this.nav_lock = false;
    }

    FrameworkModal.prototype.createModalWindow = function () {
      var _this, window, window_html;
      _this = this;
      window = null;
      if ($(".modal--window").length) {
        window = $(".modal--window");
      } else {
        window_html =
          '<div class="modal--window"> <div class="modal--mask"></div> <div class="modal--container"></div> <div class="modal--close">' +
          theme.utils.addSymbol("cross") +
          '</div> <div class="modal--prev">' +
          theme.utils.addSymbol("chevron-left") +
          '</div> <div class="modal--next">' +
          theme.utils.addSymbol("chevron-right") +
          "</div> </div>";
        window = $(window_html).appendTo("body");
      }
      return window;
    };

    FrameworkModal.prototype.openListeners = function () {
      var _this;
      _this = this;
      return _this.links.on(
        "keypress.FrameworkModal, click.FrameworkModal, quick-open",
        function (e) {
          var clicked_item;
          if (e.type === "keypress" && theme.utils.a11yClick(e) === false) {
            return false;
          }
          clicked_item = $(this);
          _this.links.each(function (index) {
            if ($(this).is(clicked_item)) {
              if (e.type === "quick-open") {
                return _this.open(index, true);
              } else {
                return _this.open(index);
              }
            }
          });
          return false;
        }
      );
    };

    FrameworkModal.prototype.open = function (index, quick_open) {
      var _this, scrolled_position;
      if (quick_open == null) {
        quick_open = false;
      }
      _this = this;
      if (_this.modal_state === "closed") {
        _this.modal_state = "opened";
        $("html").addClass("modal-open");
        _this.window.attr("data-modal--fullscreen", _this.fullscreen);
        _this.window.attr(
          "data-modal--custom-close",
          _this.custom_close_button
        );
        _this.window.attr("data-modal--view", _this.view);
        _this.closeListeners();
        _this.positionListeners();
        scrolled_position = $(window).scrollTop();
        _this.main_content_window.css({
          position: "fixed",
          top: -scrolled_position,
        });
        _this.moveContentToWindow();
        if (_this.slides.length > 1) {
          _this.next_button.show();
          _this.prev_button.show();
          _this.prevListeners();
          _this.nextListeners();
        }
        _this.window.show();
        _this.window_container.show();
        _this.renderVideo(_this.slides.eq(index));
        if (quick_open) {
          _this.slides.eq(index).addClass("active");
          return _this.position();
        } else {
          _this.mask.fadeIn();
          return _this.loadModal(_this.slides.eq(index), function () {
            return setTimeout(function () {
              return _this.window_container.find('input[type="text"]').focus();
            }, 0);
          });
        }
      }
    };

    FrameworkModal.prototype.moveContentToWindow = function (index) {
      var _this, content;
      _this = this;
      content = _this.container.find(".modal--content");
      _this.window_container.append(content);
      return (_this.slides = _this.window_container.find(".modal--content"));
    };

    FrameworkModal.prototype.detectImageSlide = function (slide) {
      var _this;
      return (_this = this);
    };

    FrameworkModal.prototype.renderVideo = function (slide) {
      var _this, id, iframe, src_url, type, video;
      _this = this;
      video = slide.find(".responsive-video").first();
      src_url = video.data("video");
      type = _this.extractVideoType(src_url);
      id = _this.extractVideoId(src_url, type);
      iframe = _this.createIframe(type, id);
      if (type === "vimeo") {
        video.addClass("vimeo");
      }
      if (type === "kickstarter") {
        video.addClass("kickstarter");
      }
      return video.append(iframe);
    };

    FrameworkModal.prototype.extractVideoType = function (src_url) {
      var _this, matches, re;
      _this = this;
      re = /\/\/(?:www\.)?youtu(?:\.be|be\.com)\/(?:watch\?v=|embed\/)?([a-z0-9_\-]+)/i;
      matches = re.exec(src_url);
      if (matches) {
        return "youtube";
      }
      re = /^.*(vimeo)\.com\/(?:watch\?v=)?(.*?)(?:\z|$|&)/;
      matches = re.exec(src_url);
      if (matches) {
        return "vimeo";
      }
      re = /^.*(kickstarter)\.com/g;
      matches = re.exec(src_url);
      if (matches) {
        return "kickstarter";
      }
      return false;
    };

    FrameworkModal.prototype.extractVideoId = function (src_url, type) {
      var _this, match, regExp;
      _this = this;
      if (type === "youtube") {
        regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        match = src_url.match(regExp);
        if (match && match[2].length === 11) {
          return match[2];
        }
      } else if (type === "vimeo") {
        regExp = /^.*(vimeo)\.com\/(?:watch\?v=)?(.*?)(?:\z|$|&)/;
        match = src_url.match(regExp);
        if (match) {
          return match[2];
        }
      } else if (type === "kickstarter") {
        regExp = /(?:kickstarter\.com\/projects\/)(.*)(|\?)/;
        match = src_url.match(regExp);
        if (match) {
          return match[1];
        }
      }
    };

    FrameworkModal.prototype.createIframe = function (type, id) {
      var _this;
      _this = this;
      if (type === "youtube") {
        return (
          '<iframe  src="//www.youtube.com/embed/' +
          id +
          '?autoplay=1" frameborder="0" allowfullwidth></iframe>'
        );
      } else if (type === "vimeo") {
        return (
          '<iframe src="//player.vimeo.com/video/' +
          id +
          '?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff&amp;autoplay=1?" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'
        );
      } else if (type === "kickstarter") {
        return (
          '<iframe src="//www.kickstarter.com/projects/' +
          id +
          '/widget/video.html" frameborder="0" webkitallowfullwidth mozallowfullwidth allowfullwidth></iframe>'
        );
      }
    };

    FrameworkModal.prototype.removeVideos = function () {
      var _this;
      _this = this;
      return _this.slides.find(".responsive-video").each(function () {
        if ($(this).data("video")) {
          return $(this).find("iframe").remove();
        }
      });
    };

    FrameworkModal.prototype.loadModal = function (modal, callback) {
      var _this;
      _this = this;
      modal.addClass("active");
      _this.position();
      if (callback) {
        callback();
      }
      return (_this.nav_lock = false);
    };

    FrameworkModal.prototype.position = function () {
      var _this, active_modal, entire_modal_height, modal_height, modal_width;
      _this = this;
      if (_this.window_container != null) {
        active_modal = _this.content.filter(".active");
        _this.window_container.attr("style", "");
        _this.positionVideoColumns(active_modal);
        _this.detectImageSlide(active_modal);
        _this.window.removeClass("fixed");
        modal_height = active_modal.outerHeight();
        modal_width = active_modal.outerWidth();
        entire_modal_height =
          modal_height +
          parseInt(_this.window.css("padding-top")) +
          parseInt(_this.window.css("padding-bottom"));
        if (_this.fullscreen) {
          return;
        }
        if (active_modal.hasClass("type--image")) {
          entire_modal_height = modal_height;
        }
        if (
          $(window).height() >= entire_modal_height &&
          _this.force_view !== "absolute"
        ) {
          return _this.window.addClass("fixed");
        } else {
          $("html, body").animate(
            {
              scrollTop: 0,
            },
            "0"
          );
          $("body,html").on(
            "DOMMouseScroll.FrameworkModal mousewheel.FrameworkModal touchmove.FrameworkModal",
            function (e) {
              if (
                e.which > 0 ||
                e.type === "mousewheel" ||
                e.type === "touchmove"
              ) {
                return $("html,body").stop();
              }
            }
          );
          return _this.window.removeClass("fixed");
        }
      }
    };

    FrameworkModal.prototype.positionVideoColumns = function (modal) {
      var _this, column_one, column_two, height_one, height_two;
      _this = this;
      if (modal.find(".responsive-video").length > 0) {
        column_one = modal.find(".fw--blocks > div").eq(0);
        column_two = modal.find(".fw--blocks > div").eq(1);
        column_one.css({
          width: "",
        });
        column_two.css({
          width: "",
        });
        height_one = column_one.outerHeight();
        height_two = column_two.outerHeight();
        if (height_one < height_two) {
          column_one.css({
            width: "100%",
          });
          return column_two.css({
            width: "100%",
          });
        }
      }
    };

    FrameworkModal.prototype.positionListeners = function () {
      var _this;
      _this = this;
      return $(window).on("resize.FrameworkModal", function () {
        return _this.position();
      });
    };

    FrameworkModal.prototype.nextListeners = function () {
      var _this;
      _this = this;
      $(document).on("keydown.FrameworkModal", function (e) {
        if (e.keyCode === 39) {
          return _this.next();
        }
      });
      return _this.next_button.on("click.FrameworkModal", function () {
        return _this.next();
      });
    };

    FrameworkModal.prototype.next = function () {
      var _this, active_slide, index;
      _this = this;
      if (!_this.nav_lock) {
        _this.nav_lock = true;
        index = _this.slides.filter(".active").index();
        _this.slides.removeClass("active");
        _this.removeVideos();
        if (index + 1 === _this.slides.length) {
          active_slide = _this.slides.eq(0);
        } else {
          active_slide = _this.slides.eq(index + 1);
        }
        _this.renderVideo(active_slide);
        return _this.loadModal(active_slide);
      }
    };

    FrameworkModal.prototype.prevListeners = function () {
      var _this;
      _this = this;
      $(document).on("keydown.FrameworkModal", function (e) {
        if (e.keyCode === 37) {
          return _this.prev();
        }
      });
      return _this.prev_button.on("click.FrameworkModal", function () {
        return _this.prev();
      });
    };

    FrameworkModal.prototype.prev = function () {
      var _this, active_slide, index;
      _this = this;
      if (!_this.nav_lock) {
        _this.nav_lock = true;
        index = _this.slides.filter(".active").index();
        _this.slides.removeClass("active");
        _this.removeVideos();
        if (index === 0) {
          active_slide = _this.slides.eq(_this.slides.length - 1);
        } else {
          active_slide = _this.slides.eq(index - 1);
        }
        _this.renderVideo(active_slide);
        return _this.loadModal(active_slide);
      }
    };

    FrameworkModal.prototype.closeListeners = function () {
      var _this;
      _this = this;
      _this.container.on("quick-close", function () {
        return _this.close(true);
      });
      $(document).on("keydown.FrameworkModal", function (e) {
        if (e.keyCode === 27) {
          return _this.close();
        }
      });
      _this.mask.on("click.FrameworkModal", function () {
        return _this.close();
      });
      _this.window_container.on("click.FrameworkModal", function () {
        return _this.close();
      });
      _this.content.on("click.FrameworkModal", function (e) {
        return e.stopPropagation();
      });
      return _this.close_button.on("click.FrameworkModal", function () {
        return _this.close();
      });
    };

    FrameworkModal.prototype.close = function (quick_close) {
      var _this, scrolled_position;
      if (quick_close == null) {
        quick_close = false;
      }
      _this = this;
      scrolled_position = parseInt(_this.main_content_window.css("top")) * -1;
      _this.container.trigger("modalClosed");
      _this.main_content_window.css({
        top: "0",
        position: "relative",
      });
      $(window).scrollTop(scrolled_position);
      _this.putBackContent();
      _this.next_button.hide();
      _this.prev_button.hide();
      _this.window.hide();
      if (quick_close) {
        _this.mask.hide();
        this.window_container.empty();
        _this.modal_state = "closed";
      } else {
        _this.mask.fadeOut(function () {
          _this.window_container.empty();
          return (_this.modal_state = "closed");
        });
      }
      $("html").removeClass("modal-open");
      _this.removeListeners();
      return $(window).trigger("FrameworkModal.afterClose");
    };

    FrameworkModal.prototype.putBackContent = function () {
      var _this;
      _this = this;
      _this.removeVideos();
      return _this.slides.removeClass("active").appendTo(_this.container);
    };

    FrameworkModal.prototype.removeListeners = function () {
      var _this;
      _this = this;
      $(document).off("keydown.FrameworkModal");
      $(window).off("resize.FrameworkModal");
      $("body,html").off(
        "DOMMouseScroll.FrameworkModal mousewheel.FrameworkModal touchmove.FrameworkModal"
      );
      _this.next_button.off("click.FrameworkModal");
      _this.prev_button.off("click.FrameworkModal");
      _this.close_button.off("click.FrameworkModal");
      _this.mask.off("click.FrameworkModal");
      return _this.window_container.off("click.FrameworkModal");
    };

    return FrameworkModal;
  })();

  theme.classes.FrameworkOffCanvas = (function () {
    function FrameworkOffCanvas(root) {
      var _this;
      this.root = root;
      this.touchListener = bind(this.touchListener, this);
      this.closeRight = bind(this.closeRight, this);
      this.closeLeft = bind(this.closeLeft, this);
      this.fadeInOverlay = bind(this.fadeInOverlay, this);
      this.openRight = bind(this.openRight, this);
      this.openLeft = bind(this.openLeft, this);
      this.toggle = bind(this.toggle, this);
      this.toggleListeners = bind(this.toggleListeners, this);
      this.viewPortHeightListener = bind(this.viewPortHeightListener, this);
      this.setViewPortHeight = bind(this.setViewPortHeight, this);
      this.setState = bind(this.setState, this);
      this.unload = bind(this.unload, this);
      this.load = bind(this.load, this);
      _this = this;
      _this.viewport = $(".off-canvas--viewport");
      _this.left_sidebar = $(".off-canvas--left-sidebar");
      _this.right_sidebar = $(".off-canvas--right-sidebar");
      _this.main_content = $(".off-canvas--main-content");
      _this.overlay = $(".off-canvas--overlay");
      _this.state = "closed";
      _this.load();
    }

    FrameworkOffCanvas.prototype.load = function () {
      var _this;
      _this = this;
      _this.close = $(".off-canvas--close");
      _this.triggers = $(".off-canvas--open");
      _this.setViewPortHeight();
      _this.viewPortHeightListener();
      _this.toggleListeners();
      return _this.touchListener();
    };

    FrameworkOffCanvas.prototype.unload = function () {
      var _this;
      _this = this;
      _this.triggers.off("click");
      _this.overlay.add(_this.close).off("click");
      $(document).off("touchstart.FrameworkOffCanvas");
      return $(document).off("touchend.FrameworkOffCanvas");
    };

    FrameworkOffCanvas.prototype.setState = function (state) {
      var _this;
      _this = this;
      _this.state = state;
      return _this.root.attr("data-off-canvas--state", state);
    };

    FrameworkOffCanvas.prototype.setViewPortHeight = function () {
      var _this;
      _this = this;
      return _this.viewport.css({
        "min-height": $(window).height(),
      });
    };

    FrameworkOffCanvas.prototype.viewPortHeightListener = function () {
      var _this;
      _this = this;
      return $(window).on("resize", function () {
        return _this.setViewPortHeight();
      });
    };

    FrameworkOffCanvas.prototype.toggleListeners = function () {
      var _this;
      _this = this;
      _this.triggers.on("click", function () {
        if ($(this).data("off-canvas--open") === "left-sidebar") {
          _this.toggle("left-sidebar");
        } else if ($(this).data("off-canvas--open") === "right-sidebar") {
          _this.toggle("right-sidebar");
        }
        return false;
      });
      return _this.overlay.add(_this.close).on("click", function () {
        if (_this.state === "left-open") {
          return _this.toggle("left-sidebar");
        } else if (_this.state === "right-open") {
          return _this.toggle("right-sidebar");
        }
      });
    };

    FrameworkOffCanvas.prototype.toggle = function (element) {
      var _this;
      _this = this;
      if (element === "left-sidebar" && _this.state === "closed") {
        return _this.openLeft();
      } else if (element === "left-sidebar" && _this.state === "left-open") {
        return _this.closeLeft();
      } else if (element === "right-sidebar" && _this.state === "closed") {
        return _this.openRight();
      } else if (element === "right-sidebar" && _this.state === "right-open") {
        return _this.closeRight();
      }
    };

    FrameworkOffCanvas.prototype.openLeft = function () {
      var _this;
      _this = this;
      _this.setState("left-open");
      _this.main_content.css({
        position: "fixed",
        top: -$(window).scrollTop(),
      });
      _this.left_sidebar.velocity(
        {
          translateX: [0, "-100%"],
        },
        {
          complete: function () {
            return _this.left_sidebar.css({
              position: "fixed",
            });
          },
        }
      );
      return _this.fadeInOverlay();
    };

    FrameworkOffCanvas.prototype.openRight = function () {
      var _this;
      _this = this;
      _this.setState("right-open");
      _this.main_content.css({
        position: "fixed",
        top: -$(window).scrollTop(),
      });
      _this.right_sidebar.velocity(
        {
          translateX: ["-100%", 0],
        },
        {
          complete: function () {
            return _this.right_sidebar.css({
              position: "fixed",
            });
          },
        }
      );
      return _this.fadeInOverlay();
    };

    FrameworkOffCanvas.prototype.fadeInOverlay = function () {
      var _this;
      _this = this;
      _this.overlay.show();
      return _this.overlay.velocity({
        opacity: "0.3",
      });
    };

    FrameworkOffCanvas.prototype.closeLeft = function () {
      var _this, scrolled_position;
      _this = this;
      if (_this.state !== "left-open") {
        return false;
      }
      _this.setState("closed");
      scrolled_position = parseInt(_this.main_content.css("top")) * -1;
      _this.left_sidebar.velocity(
        {
          translateX: ["-100%", 0],
        },
        {
          complete: function () {
            _this.left_sidebar.css({
              position: "absolute",
            });
            _this.main_content.css({
              position: "fixed",
              top: "initial",
            });
            return $(window).scrollTop(scrolled_position);
          },
        }
      );
      return _this.overlay.velocity("fadeOut");
    };

    FrameworkOffCanvas.prototype.closeRight = function () {
      var _this, scrolled_position;
      _this = this;
      if (_this.state !== "right-open") {
        return false;
      }
      _this.setState("closed");
      scrolled_position = parseInt(_this.main_content.css("top")) * -1;
      _this.right_sidebar.velocity(
        {
          translateX: [0, "-100%"],
        },
        {
          complete: function () {
            _this.right_sidebar.css({
              position: "absolute",
            });
            _this.main_content.css({
              position: "relative",
              top: "initial",
            });
            return $(window).scrollTop(scrolled_position);
          },
        }
      );
      return _this.overlay.velocity("fadeOut");
    };

    FrameworkOffCanvas.prototype.touchListener = function () {
      var _this, position;
      _this = this;
      if (!Modernizr.touchevents) {
        return false;
      }
      position = {
        start: {},
        end: {},
      };
      $(document).on("touchstart.FrameworkOffCanvas", function (e) {
        position.start.x = e.originalEvent.touches[0].clientX;
        position.start.y = e.originalEvent.touches[0].clientY;
      });
      return $(document).on("touchend.FrameworkOffCanvas", function (e) {
        position.end.x = e.originalEvent.changedTouches[0].clientX;
        position.end.y = e.originalEvent.changedTouches[0].clientY;
        if (Math.abs(position.start.y - position.end.y) > 30) {
          return false;
        }
        if (position.start.x > position.end.x + 5) {
          return _this.closeLeft();
        } else if (position.start.x < position.end.x - 5) {
          return _this.closeRight();
        }
      });
    };

    return FrameworkOffCanvas;
  })();

  theme.classes.FrameworkProductRecommendations = (function () {
    function FrameworkProductRecommendations(root) {
      var _this;
      this.root = root;
      this.loadHoverImages = bind(this.loadHoverImages, this);
      this.checkIfEmpty = bind(this.checkIfEmpty, this);
      this.loadProducts = bind(this.loadProducts, this);
      this.resizeListeners = bind(this.resizeListeners, this);
      this.matchImageHeights = bind(this.matchImageHeights, this);
      _this = this;
      _this.loadProducts();
      _this.resizeListeners();
    }

    FrameworkProductRecommendations.prototype.matchImageHeights = function () {
      var _this;
      _this = this;
      _this.item_container = _this.root.find(".product-recommendations--body");
      _this.items = _this.root.find(".product--root");
      return theme.utils.matchImageHeights(
        _this.item_container,
        _this.items,
        ".product--image-wrapper"
      );
    };

    FrameworkProductRecommendations.prototype.resizeListeners = function () {
      var _this;
      _this = this;
      return $(window).on(
        "resize",
        theme.utils.debounce(100, function () {
          return _this.matchImageHeights();
        })
      );
    };

    FrameworkProductRecommendations.prototype.loadProducts = function () {
      var _this,
        maxProducts,
        productId,
        productRecommendations,
        request,
        requestUrl;
      _this = this;
      productRecommendations = document.getElementsByClassName(
        "product-recommendations--root"
      )[0];
      productId = productRecommendations.dataset.productId;
      maxProducts = productRecommendations.dataset.maxProducts;
      requestUrl =
        "/recommendations/products?section_id=framework--product-recommendations&limit=" +
        maxProducts +
        "&product_id=" +
        productId;
      request = new XMLHttpRequest();
      request.open("GET", requestUrl);
      request.onload = function () {
        var container;
        if (request.status >= 200 && request.status < 300) {
          container = document.createElement("div");
          container.innerHTML = request.response;
          productRecommendations.innerHTML = container.querySelector(
            ".product-recommendations--root"
          ).innerHTML;
          _this.matchImageHeights();
          _this.checkIfEmpty();
          return _this.loadHoverImages();
        }
      };
      return request.send();
    };

    FrameworkProductRecommendations.prototype.checkIfEmpty = function () {
      var _this, empty;
      _this = this;
      empty = $(".product--root").length > 0 ? false : true;
      return $(".product-recommendations--root").attr("data-empty", empty);
    };

    FrameworkProductRecommendations.prototype.loadHoverImages = function () {
      var _this;
      _this = this;
      return $('.product--root[data-hover-image="true"]').each(function () {
        var product;
        product = $(this);
        return theme.utils.imagesLoaded(product, function () {
          return product.attr("data-hover-image", "loaded");
        });
      });
    };

    return FrameworkProductRecommendations;
  })();

  theme.classes.FrameworkSearch = (function () {
    function FrameworkSearch(root) {
      var _this;
      this.root = root;
      this.resizeListeners = bind(this.resizeListeners, this);
      this.matchImageHeights = bind(this.matchImageHeights, this);
      this.getResults = bind(this.getResults, this);
      this.listenForKeyEntered = bind(this.listenForKeyEntered, this);
      this.getSearchUrl = bind(this.getSearchUrl, this);
      this.searchLinks = bind(this.searchLinks, this);
      // this.loadSpinners = bind(this.loadSpinners, this);
      this.onOpen = bind(this.onOpen, this);
      _this = this;
      _this.articles = _this.root.find(".search--articles");
      _this.form = _this.root.find("form");
      _this.icon = _this.root.find(".search--icon");
      _this.loading = _this.root.find(".search--loading");
      _this.no_results = _this.root.find(".search--no-results");
      _this.products = _this.root.find(".search--products");
      _this.results = _this.root.find(".search--results");
      _this.text_box = _this.root.find(".search--textbox");
      _this.toggle_link = _this.root.find(".search--toggle");
      _this.show_articles = _this.root.data("show-articles");
      _this.show_pages = _this.root.data("show-pages");
      _this.view = _this.root.data("view");
      _this.ajax_request = null;
      _this.search_term = null;
      _this.search_url = null;
      _this.close_results_timer = null;
      _this.typing_timer = null;
      _this.article_page_combination = "";
      _this.offCanvas = null;
      if (theme.partials.OffCanvas !== void 0) {
        _this.offCanvas = theme.partials.OffCanvas;
      } else {
        _this.offCanvas = theme.partials.FrameworkOffCanvas;
      }
      if (_this.show_articles && _this.show_pages) {
        _this.article_page_combination = "article,page";
      } else if (_this.show_articles) {
        _this.article_page_combination = "article";
      } else if (_this.show_pages) {
        _this.article_page_combination = "page";
      }
      if (_this.view === "modal") {
        _this.searchLinks();
        _this.getSearchUrl();
        _this.listenForKeyEntered();
        // _this.loadSpinners();
      }
      _this.resizeListeners();
      _this.matchImageHeights();
    }

    FrameworkSearch.prototype.onOpen = function () {
      var _this, temp_val;
      _this = this;
      _this.text_box.focus();
      temp_val = _this.text_box.val();
      _this.text_box.val("");
      _this.text_box.val(temp_val);
      return _this.text_box.trigger("keyup");
    };

    // FrameworkSearch.prototype.loadSpinners = function () {
    //   var _this, spinner;
    //   _this = this;
    //   spinner = _this.loading.find(".animation");
    //   if (spinner.hasClass("tiny")) {
    //     return spinner.spin("tiny");
    //   } else {
    //     return spinner.spin("small");
    //   }
    // };

    FrameworkSearch.prototype.searchLinks = function () {
      var _this;
      _this = this;
      $('.off-canvas--main-content a[href="/search"]').on("click", function () {
        $('[data-trigger="search-modal"]').trigger("click");
        _this.onOpen();
        return false;
      });
      $('.off-canvas--right-sidebar a[href="/search"]').on(
        "click",
        function () {
          _this.offCanvas.closeRight();
          setTimeout(function () {
            $('[data-trigger="search-modal"]').trigger("click");
            return _this.onOpen();
          }, 450);
          return false;
        }
      );
      return $('.off-canvas--left-sidebar a[href="/search"]').on(
        "click",
        function () {
          _this.offCanvas.closeLeft();
          setTimeout(function () {
            $('[data-trigger="search-modal"]').trigger("click");
            return _this.onOpen();
          }, 450);
          return false;
        }
      );
    };

    FrameworkSearch.prototype.getSearchUrl = function () {
      var _this;
      _this = this;
      _this.search_url = window.location.href;
      _this.search_url = _this.search_url.replace(
        window.location.hostname + window.location.pathname,
        window.location.hostname + "/search"
      );
      _this.search_url = _this.search_url.replace("#", "");
      if (_this.search_url.indexOf("?") >= 0) {
        return (_this.search_url = _this.search_url + "&");
      } else {
        return (_this.search_url = _this.search_url + "?");
      }
    };

    FrameworkSearch.prototype.listenForKeyEntered = function () {
      var _this;
      _this = this;
      return _this.text_box
        .attr("autocomplete", "off")
        .on("keyup paste", function (event) {
          var term;
          clearTimeout(_this.typing_timer);
          term = $(this).val();
          if (term.length < 2 && event.type !== "paste") {
            _this.products.empty();
            _this.articles.empty();
            _this.loading.hide();
            _this.icon.show();
            return false;
          }
          _this.loading.show();
          _this.icon.hide();
          return (_this.typing_timer = setTimeout(function () {
            var url;
            clearTimeout(_this.typing_timer);
            url =
              _this.search_url +
              "view=ajax-product&type=product&q=" +
              term +
              "*";
            _this.getResults(url, term, "product");
            if (_this.show_articles || _this.show_pages) {
              url =
                _this.search_url +
                "view=ajax-article-page&type=" +
                _this.article_page_combination +
                "&q=" +
                term +
                "*";
              return _this.getResults(url, term, "article");
            }
          }, 750));
        });
    };

    FrameworkSearch.prototype.getResults = function (url, term, type) {
      var _this;
      _this = this;
      _this.products.empty();
      _this.articles.empty();
      $.ajax({
        url: url,
        type: "GET",
        dataType: "html",
        success: function (data) {
          _this.loading.hide();
          _this.icon.show();
          if (type === "product") {
            _this.products.empty();
            _this.products.append(data);
            return _this.matchImageHeights();
          } else if (type === "article") {
            _this.articles.empty();
            return _this.articles.append(data);
          }
        },
        error: function (jqxhr, textStatus, error) {
          var err;
          err = textStatus + ", " + error;
          return console.log("search.json Request Failed: " + err);
        },
      });
    };

    FrameworkSearch.prototype.matchImageHeights = function () {
      var _this;
      _this = this;
      return theme.utils.matchImageHeights(
        _this.products,
        _this.products.find(".product--root"),
        ".product--image-wrapper"
      );
    };

    FrameworkSearch.prototype.resizeListeners = function () {
      var _this;
      _this = this;
      return $(window).on(
        "resize.Search",
        theme.utils.debounce(100, function () {
          return _this.matchImageHeights();
        })
      );
    };

    return FrameworkSearch;
  })();

  theme.classes.Sections = (function () {
    function Sections() {
      this.unload = bind(this.unload, this);
      this.deselectBlock = bind(this.deselectBlock, this);
      this.selectBlock = bind(this.selectBlock, this);
      this.deselectSection = bind(this.deselectSection, this);
      this.selectSection = bind(this.selectSection, this);
      this.load = bind(this.load, this);
      this.getActiveBlock = bind(this.getActiveBlock, this);
      this.getActiveSection = bind(this.getActiveSection, this);
      this.listeners = bind(this.listeners, this);
      var _this;
      _this = this;
      _this.listeners();
    }

    Sections.prototype.listeners = function () {
      var _this;
      _this = this;
      _this.load();
      _this.unload();
      _this.selectSection();
      _this.deselectSection();
      _this.selectBlock();
      return _this.deselectBlock();
    };

    Sections.prototype.getActiveSection = function (evt) {
      var _this, active_section;
      _this = this;
      active_section = $(evt.target).find("[data-section-id]");
      return active_section;
    };

    Sections.prototype.getActiveBlock = function (evt) {
      var _this, active_block;
      _this = this;
      active_block = $(evt.target);
      return active_block;
    };

    Sections.prototype.load = function (evt) {
      var _this;
      _this = this;
      return $(document).on("shopify:section:load", function (evt) {
        var active_section;
        theme.utils.loadJsClasses();
        active_section = _this.getActiveSection(evt);
        active_section.triggerHandler("theme:section:load");
        return active_section.find("[data-js-class]").each(function () {
          return $(this).triggerHandler("theme:section:load");
        });
      });
    };

    Sections.prototype.selectSection = function () {
      var _this;
      _this = this;
      return $(document).on("shopify:section:select", function (evt) {
        var active_section;
        active_section = _this.getActiveSection(evt);
        return active_section.triggerHandler("theme:section:select");
      });
    };

    Sections.prototype.deselectSection = function () {
      var _this;
      _this = this;
      return $(document).on("shopify:section:deselect", function (evt) {
        var active_section;
        active_section = _this.getActiveSection(evt);
        return active_section.triggerHandler("theme:section:deselect");
      });
    };

    Sections.prototype.selectBlock = function () {
      var _this;
      _this = this;
      return $(document).on("shopify:block:select", function (evt) {
        var active_block;
        active_block = _this.getActiveBlock(evt);
        return active_block.triggerHandler("theme:block:select");
      });
    };

    Sections.prototype.deselectBlock = function () {
      var _this;
      _this = this;
      return $(document).on("shopify:block:deselect", function (evt) {
        var active_block;
        active_block = _this.getActiveBlock(evt);
        return active_block.triggerHandler("theme:block:deselect");
      });
    };

    Sections.prototype.unload = function (evt) {
      var _this;
      _this = this;
      return $(document).on("shopify:section:unload", function (evt) {
        var active_section;
        active_section = _this.getActiveSection(evt);
        active_section.triggerHandler("theme:section:unload");
        return active_section.find('[data-js-loaded="true"]').each(function () {
          return $(this).triggerHandler("theme:section:unload");
        });
      });
    };

    return Sections;
  })();

  theme.classes.FrameworkSlider = (function () {
    function FrameworkSlider(container1) {
      this.container = container1;
      this.eventListeners = bind(this.eventListeners, this);
      this.alignPlayButton = bind(this.alignPlayButton, this);
      this.updateContextMenu = bind(this.updateContextMenu, this);
      this.autoplay = bind(this.autoplay, this);
      this.formatPaginationNumbers = bind(this.formatPaginationNumbers, this);
      this.getActiveIndex = bind(this.getActiveIndex, this);
      this.createSlider = bind(this.createSlider, this);
      this.autoplay_enabled = this.container.data("autoplay") ? true : false;
      this.navigation = this.container.data("navigation") ? true : false;
      this.pagination_numbers = this.container.data("pagination-numbers")
        ? true
        : false;
      this.autoplay_frequency = 3000;
      this.slide_length = this.container.children().length;
      this.active_index = 0;
      this.news_panel = this.container.data("news-panel") ? true : false;
      this.createSlider();
      this.eventListeners();
      this.owl = this.container.data("owlCarousel");
    }

    FrameworkSlider.prototype.createSlider = function () {
      var _this, slider;
      _this = this;
      slider = this.container.owlCarousel({
        singleItem: true,
        navigation: _this.navigation,
        navigationText: false,
        pagination: _this.container.data("pagination") ? true : false,
        paginationNumbers: _this.pagination_numbers,
        scrollPerPageNav: true,
        slideSpeed: 800,
        autoHeight: false,
        autoPlay: _this.autoplay(),
        afterInit: function () {},
        afterAction: function () {
          _this.alignPlayButton();
          _this.active_index = _this.getActiveIndex();
          _this.updateContextMenu(_this.active_index);
          return _this.formatPaginationNumbers();
        },
      });
      if (_this.navigation) {
        slider.find(".owl-prev").html(theme.utils.addSymbol("chevron-left"));
        slider.find(".owl-next").html(theme.utils.addSymbol("chevron-right"));
      }
      return slider;
    };

    FrameworkSlider.prototype.getActiveIndex = function () {
      return this.container.find(".owl-pagination .owl-page.active").index();
    };

    FrameworkSlider.prototype.formatPaginationNumbers = function () {
      return this.container
        .find(".owl-page.active .owl-numbers")
        .text(this.active_index + 1 + "/" + this.slide_length);
    };

    FrameworkSlider.prototype.autoplay = function () {
      if (this.autoplay_enabled) {
        return this.autoplay_frequency;
      }
      return false;
    };

    FrameworkSlider.prototype.updateContextMenu = function (index) {
      var context_navigation, type_class;
      if (this.news_panel) {
        type_class =
          "." + this.container.find(".slide").eq(index).data("feed-type");
        context_navigation = this.container
          .closest(".template--index--news")
          .find(".context-navigation");
        context_navigation.find(".item").hide();
        return context_navigation.find(type_class).show();
      }
    };

    FrameworkSlider.prototype.alignPlayButton = function () {
      var play_button, play_button_height, slide, slide_height, video_offset;
      slide = this.container.find(".owl-item").eq(this.active_index);
      play_button = slide.find(".play-button");
      if (play_button.length === 0) {
        return;
      }
      play_button.css("visibility", "hidden");
      if (
        PAGE.hasClass("transparent-menu") &&
        $(".header--root").css("position") === "absolute"
      ) {
        slide_height = slide.outerHeight();
        play_button_height = play_button.outerHeight();
        video_offset = (slide_height - play_button_height) / 2;
        play_button.css({
          "margin-top": 0,
          top: video_offset,
        });
      } else {
        play_button.css({
          "margin-top": "-40px",
          top: "50%",
        });
      }
      return play_button.css("visibility", "visible");
    };

    FrameworkSlider.prototype.eventListeners = function () {
      var _this;
      _this = this;
      this.container.find(".play-button").on("click", function () {
        var video_modal;
        video_modal = new VideoModal($(this).closest(".video"));
        video_modal.open();
        _this.owl.stop();
        return false;
      });
      return this.container
        .find(".owl-pagination .owl-page, .skip-to-next")
        .on("click", function () {
          _this.owl.next();
          return false;
        });
    };

    return FrameworkSlider;
  })();

  theme.classes.FrameworkStickyColumn = (function () {
    function FrameworkStickyColumn(container1, column_a, column_b, mq) {
      this.container = container1;
      this.column_a = column_a;
      this.column_b = column_b;
      this.mq = mq;
      this.Listeners = bind(this.Listeners, this);
      this.setColumnPosition = bind(this.setColumnPosition, this);
      this.getAlignment = bind(this.getAlignment, this);
      this.getState = bind(this.getState, this);
      this.resetLargerColumn = bind(this.resetLargerColumn, this);
      this.getSmallerColumn = bind(this.getSmallerColumn, this);
      this.heightsHaveChanged = bind(this.heightsHaveChanged, this);
      this.setHeights = bind(this.setHeights, this);
      this.loadColumns = bind(this.loadColumns, this);
      if (Modernizr.touchevents) {
        return false;
      }
      this.current_state = "auto";
      this.column_a_height = 0;
      this.column_b_height = 0;
      this.loadColumns();
    }

    FrameworkStickyColumn.prototype.loadColumns = function () {
      var _this;
      _this = this;
      return theme.utils.imagesLoaded(_this.container, function () {
        _this.Listeners();
        return _this.setColumnPosition();
      });
    };

    FrameworkStickyColumn.prototype.setHeights = function () {
      this.column_a_height = this.column_a.outerHeight();
      return (this.column_b_height = this.column_b.outerHeight());
    };

    FrameworkStickyColumn.prototype.heightsHaveChanged = function () {
      if (this.column_a.outerHeight() !== this.column_a_height) {
        this.setHeights();
        return true;
      }
      if (this.column_b.outerHeight() !== this.column_b_height) {
        this.setHeights();
        return true;
      }
      return false;
    };

    FrameworkStickyColumn.prototype.getSmallerColumn = function () {
      if (this.column_a_height < this.column_b_height) {
        return this.column_a;
      } else {
        return this.column_b;
      }
    };

    FrameworkStickyColumn.prototype.resetLargerColumn = function () {
      if (this.column_a_height > this.column_b_height) {
        return this.column_a.css({
          position: "relative",
          top: "auto",
          bottom: "auto",
        });
      } else {
        return this.column_b.css({
          position: "relative",
          top: "auto",
          bottom: "auto",
        });
      }
    };

    FrameworkStickyColumn.prototype.getState = function (
      scroll_pos,
      window_height,
      column
    ) {
      var column_height, height_for_bottom, overflowing_column, state;
      state = "auto";
      if (this.mq.current_window === "small") {
        return "auto";
      }
      column_height = column.outerHeight();
      if (window_height > column_height) {
        overflowing_column = true;
      }
      if (scroll_pos < this.container.offset().top) {
        state = "auto";
      }
      if (overflowing_column) {
        height_for_bottom = column_height;
      } else {
        height_for_bottom = window_height;
      }
      if (
        scroll_pos + height_for_bottom >
        this.container.offset().top + this.container.outerHeight()
      ) {
        state = "absolute-bottom";
      } else if (
        scroll_pos > this.container.offset().top &&
        overflowing_column
      ) {
        state = "fixed-top";
      } else if (
        window_height < column_height &&
        scroll_pos + window_height >
          this.container.offset().top + column.outerHeight()
      ) {
        state = "fixed-bottom";
      }
      return state;
    };

    FrameworkStickyColumn.prototype.getAlignment = function (column) {
      if (column.hasClass("column-a")) {
        return "left";
      } else if (column.hasClass("column-b")) {
        return "right";
      }
    };

    FrameworkStickyColumn.prototype.setColumnPosition = function () {
      var _this, align, column, state;
      _this = this;
      _this.setHeights();
      column = _this.getSmallerColumn();
      state = _this.getState($(window).scrollTop(), $(window).height(), column);
      align = _this.getAlignment(column);
      if (state === "auto" && this.current_state !== "auto") {
        this.current_state = "auto";
        column.css({
          position: "relative",
          top: "auto",
          bottom: "auto",
        });
      } else if (
        state === "fixed-bottom" &&
        this.current_state !== "fixed-bottom"
      ) {
        this.current_state = "fixed-bottom";
        column.css({
          position: "fixed",
          top: "auto",
          bottom: 0,
        });
      } else if (state === "fixed-top" && this.current_state !== "fixed-top") {
        this.current_state = "fixed-top";
        column.css({
          position: "fixed",
          top: 0,
          bottom: "auto",
        });
      } else if (
        state === "absolute-bottom" &&
        this.current_state !== "absolute-bottom"
      ) {
        this.current_state = "absolute-bottom";
        column.css({
          position: "absolute",
          top: "auto",
          bottom: 0,
        });
      }
      if (align === "right") {
        return column.css({
          right: 0,
        });
      }
    };

    FrameworkStickyColumn.prototype.Listeners = function () {
      var _this;
      _this = this;
      $(window).on("scroll", function () {
        return _this.setColumnPosition();
      });
      setInterval(function () {
        if (_this.heightsHaveChanged()) {
          _this.resetLargerColumn();
          return _this.setColumnPosition();
        }
      }, 250);
      return $(window).on("resize", function () {
        _this.resetLargerColumn();
        return _this.setColumnPosition();
      });
    };

    return FrameworkStickyColumn;
  })();

  theme.classes.FrameworkUtils = (function () {
    function FrameworkUtils() {
      var _this;
      _this = this;
      _this.google_map_api_status = null;
      //cssVars();
    }

    FrameworkUtils.prototype.debounce = function (delay, fn) {
      var timeoutId;
      timeoutId = void 0;
      return function () {
        if (timeoutId) {
          clearTimeout(timeoutId);
        }
        timeoutId = setTimeout(fn.bind(this), delay, arguments);
      };
    };

    FrameworkUtils.prototype.imagesLoaded = function (selector, callback) {
      var count, images_length;
      count = 0;
      images_length = selector.find('img[data-sizes="auto"]').length;
      if (images_length < 1) {
        callback();
        return;
      }
      return selector.on("lazybeforeunveil", function (e) {
        return $(e.target).one("load", function () {
          count++;
          if (count === images_length) {
            return callback();
          }
        });
      });
    };

    FrameworkUtils.prototype.a11yClick = function (event) {
      var code;
      if (event.type === "click") {
        return true;
      } else if (event.type === "keypress") {
        code = event.charCode || event.keyCode;
        if (code === 32) {
          event.preventDefault();
        }
        if (code === 32 || code === 13) {
          return true;
        }
      }
      return false;
    };

    FrameworkUtils.prototype.matchImageHeights = function (
      container,
      items,
      image_class
    ) {
      var _this, greatest_image_height, items_per_row, row_items;
      _this = this;
      items_per_row = Math.round(
        container.width() / items.first().outerWidth()
      );
      greatest_image_height = 0;
      row_items = $();
      items.find(image_class).css("height", "auto");
      items.find(".placeholder--root").css("height", "auto");
      return items.each(function (index) {
        var this_height;
        if ($(this).find(".image--root").length > 0) {
          this_height = $(this)
            .find(image_class + " .image--root")
            .outerHeight();
        } else {
          this_height = $(this).find(".placeholder--root").outerHeight();
        }
        row_items = row_items.add($(this));
        if (this_height > greatest_image_height) {
          greatest_image_height = this_height;
        }
        if (
          index % items_per_row === items_per_row - 1 ||
          index + 1 === items.length
        ) {
          row_items
            .find(image_class + ", .placeholder--root")
            .height(greatest_image_height);
          greatest_image_height = 0;
          return (row_items = $());
        }
      });
    };

    FrameworkUtils.prototype.isNaN = function (num) {
      var _this;
      _this = this;
      return num !== num;
    };

    return FrameworkUtils;
  })();

  theme.classes.FrameworkXMenu = (function () {
    function FrameworkXMenu(root) {
      var _this;
      this.root = root;
      this.slideUp = bind(this.slideUp, this);
      this.slideDown = bind(this.slideDown, this);
      this.arrangeMegaNav = bind(this.arrangeMegaNav, this);
      this.resizeListeners = bind(this.resizeListeners, this);
      this.checkOverlap = bind(this.checkOverlap, this);
      this.listeners = bind(this.listeners, this);
      this.load = bind(this.load, this);
      _this = this;
      _this.debugging = false;
      _this.state = "closed";
      _this.parent_links = _this.root.find(".x-menu--level-1--link > a");
      _this.sub_menu_links = _this.root.find(
        '.x-menu--level-1--link:not([data-x-menu--depth="1"]) > a'
      );
      _this.sub_menu_items = _this.sub_menu_links.parent().find("ul a");
      _this.parents_with_sub_menu = _this.sub_menu_links.parent();
      _this.overlap_parent = _this.root.data("x-menu--overlap-parent");
      _this.load();
    }

    FrameworkXMenu.prototype.load = function () {
      var _this;
      _this = this;
      _this.arrangeMegaNav();
      _this.listeners();
      _this.checkOverlap();
      return _this.resizeListeners();
    };

    FrameworkXMenu.prototype.listeners = function () {
      var _this;
      _this = this;
      _this.parents_with_sub_menu.on("mouseenter.XMenu", function (e) {
        return _this.slideDown($(this).find("> a"));
      });
      _this.parents_with_sub_menu.on("mouseleave.XMenu", function () {
        return _this.slideUp();
      });
      _this.parent_links.on("focus", function (e) {
        return _this.slideUp();
      });
      _this.sub_menu_links.on("focus", function (e) {
        return _this.slideDown($(this));
      });
      return _this.sub_menu_links.on("touchstart.XMenu", function (e) {
        e.preventDefault();
        if ($(this).parent().attr("data-x-menu--open") === "true") {
          return _this.slideUp();
        } else {
          return _this.slideDown($(this));
        }
      });
    };

    FrameworkXMenu.prototype.checkOverlap = function () {
      var _this,
        center_index,
        center_item,
        center_item_left_edge,
        center_item_right_edge,
        center_item_width,
        container,
        container_width,
        first_center_child,
        last_center_child,
        left_break_point,
        left_item,
        right_item,
        right_item_edge;
      _this = this;
      if (Modernizr.touchevents && theme.utils.mqs.current_window !== "large") {
        _this.root.attr("data-x-menu--overlap", "true");
        return false;
      }
      _this.root.attr("data-x-menu--overlap", "false");
      center_item = _this.root;
      if (_this.overlap_parent === 1) {
        center_item = center_item.parent();
      } else if (_this.overlap_parent === 2) {
        center_item = center_item.parent().parent();
      }
      container = center_item.parent();
      center_index = center_item.index();
      left_item = false;
      if (center_index > 1) {
        left_item = container.children().eq(center_index - 1);
      }
      right_item = false;
      if (center_index + 1 < container.children().length) {
        right_item = container.children().eq(center_index + 1);
      }
      container_width = container.width();
      center_item_width = _this.root.outerWidth();
      if (left_item) {
        first_center_child = center_item.find("> :first-child");
        center_item_left_edge = first_center_child.offset().left - 1;
        left_break_point = (container_width - center_item_width) / 2;
        if (left_edge >= center_item_left_edge) {
          _this.root.attr("data-x-menu--overlap", "true");
        }
      }
      if (right_item) {
        last_center_child = center_item.find("> :last-child");
        center_item_right_edge =
          last_center_child.outerWidth() + last_center_child.offset().left + 1;
        right_item_edge = right_item.offset().left;
        if (center_item_right_edge >= right_item_edge) {
          return _this.root.attr("data-x-menu--overlap", "true");
        }
      }
    };

    FrameworkXMenu.prototype.resizeListeners = function () {
      var _this;
      _this = this;
      return $(window).on(
        "resize.XMenu",
        _this.debounce(100, function () {
          return _this.checkOverlap();
        })
      );
    };

    FrameworkXMenu.prototype.debounce = function (delay, fn) {
      var timeoutId;
      timeoutId = void 0;
      return function () {
        if (timeoutId) {
          clearTimeout(timeoutId);
        }
        timeoutId = setTimeout(fn.bind(this), delay, arguments);
      };
    };

    FrameworkXMenu.prototype.arrangeMegaNav = function () {
      var _this, mega_navs;
      _this = this;
      if (_this.parents_with_sub_menu.length === 0) {
        return false;
      }
      mega_navs = _this.root.find(
        '[data-x-menu--depth="3"] .x-menu--level-2--container'
      );
      return mega_navs.each(function () {
        var container, single_parents, single_parents_container;
        container = $(this);
        single_parents = container.find('[data-x-menu--single-parent="true"]');
        if (single_parents.length > 0) {
          single_parents_container = $(
            '<div class="x-menu--single-parents"></div>'
          ).insertAfter(container.find(".x-menu--bg"));
          return single_parents_container
            .append("<ul>")
            .find("ul")
            .append(single_parents);
        }
      });
    };

    FrameworkXMenu.prototype.slideDown = function (link, delay) {
      var _this, display_type, link_wrapper, menu_height, sub_menu;
      if (delay == null) {
        delay = false;
      }
      _this = this;
      clearTimeout(_this.timer);
      link_wrapper = link.parent();
      if (
        link_wrapper.attr("data-x-menu--open") === "true" ||
        _this.state === "closing"
      ) {
        return false;
      }
      _this.slideUp(false);
      if (delay && delay !== "complete") {
        _this.timer = setTimeout(function () {
          return _this.slideDown(link, "complete");
        }, delay);
      } else {
        link
          .closest(".x-menu--level-1--link")
          .find(".fw--icon--expand-less")
          .show();
        link
          .closest(".x-menu--level-1--link")
          .find(".fw--icon--expand-more")
          .hide();
        link.closest(".x-menu--level-1--link").find(".fw--icon--minus").show();
        link.closest(".x-menu--level-1--link").find(".fw--icon--plus").hide();
        _this.state = "open";
        link_wrapper.attr("data-x-menu--open", "true");
        link.attr("aria-expanded", "true");
        sub_menu = link
          .closest(".x-menu--level-1--link")
          .find(".x-menu--level-2--container");
        display_type = "block";
        if (link_wrapper.attr("data-x-menu--depth") === "3") {
          display_type = "flex";
        }
        sub_menu.velocity("stop");
        sub_menu.css({
          height: "auto",
          display: display_type,
        });
        sub_menu.find(".x-menu--level-2--list").css({
          display: display_type,
        });
        menu_height = sub_menu.outerHeight();
        sub_menu.css({
          height: 0,
          opacity: 1,
        });
        sub_menu.velocity(
          {
            height: [menu_height, 0],
          },
          {
            queue: false,
            duration: 600,
            easing: "easeOutExpo",
          }
        );
      }
    };

    FrameworkXMenu.prototype.slideUp = function (delay) {
      var _this, link, link_wrapper, sub_menu;
      if (delay == null) {
        delay = 300;
      }
      _this = this;
      if (_this.debugging) {
        return false;
      }
      link_wrapper = _this.parents_with_sub_menu.filter(
        '[data-x-menu--open="true"]'
      );
      link = link_wrapper.find("> a");
      if (link_wrapper.attr("data-x-menu--open") === "false") {
        return false;
      }
      if (delay) {
        return (_this.timer = setTimeout(function () {
          return _this.slideUp(false);
        }, delay));
      } else {
        link
          .closest(".x-menu--level-1--link")
          .find(".fw--icon--expand-less")
          .hide();
        link
          .closest(".x-menu--level-1--link")
          .find(".fw--icon--expand-more")
          .show();
        link.closest(".x-menu--level-1--link").find(".fw--icon--minus").hide();
        link.closest(".x-menu--level-1--link").find(".fw--icon--plus").show();
        sub_menu = link
          .closest(".x-menu--level-1--link")
          .find(".x-menu--level-2--container");
        link_wrapper.attr("data-x-menu--open", "false");
        link.attr("aria-expanded", "false");
        return sub_menu.velocity(
          {
            opacity: 0,
          },
          {
            begin: function () {
              return (_this.state = "closing");
            },
            complete: function () {
              sub_menu.css({
                display: "none",
              });
              return (_this.state = "closed");
            },
            duration: 200,
          }
        );
      }
    };

    return FrameworkXMenu;
  })();

  theme.classes.FrameworkYMenu = (function () {
    function FrameworkYMenu(root) {
      var _this;
      this.root = root;
      this.slideRight = bind(this.slideRight, this);
      this.slideLeft = bind(this.slideLeft, this);
      this.adjustHeight = bind(this.adjustHeight, this);
      this.listeners = bind(this.listeners, this);
      this.load = bind(this.load, this);
      _this = this;
      _this.state = "closed";
      _this.sub_menu_links = _this.root
        .find(".fw--icon--chevron-right")
        .parent();
      _this.back_links = _this.root.find(".y-menu--back-link a");
      _this.regular_links = _this.root.find(
        '.y-menu--level-1--link > a:not([data-submenu="true"]), .y-menu--level-2--link > a:not([data-submenu="true"]), .y-menu--level-3--link > a:not([data-submenu="true"])'
      );
      _this.timer = null;
      _this.load();
    }

    FrameworkYMenu.prototype.load = function () {
      var _this;
      _this = this;
      return _this.listeners();
    };

    FrameworkYMenu.prototype.listeners = function () {
      var _this;
      _this = this;
      _this.regular_links.on("click", function (e) {
        var href;
        href = $(this).attr("href");
        if (href.indexOf("#") !== -1) {
          if (theme.partials.FrameworkOffCanvas.state === "left-open") {
            theme.partials.FrameworkOffCanvas.closeLeft();
          } else if (theme.partials.FrameworkOffCanvas.state === "right-open") {
            theme.partials.FrameworkOffCanvas.closeRight();
          }
          setTimeout(function () {
            return (window.location.href = href);
          }, 450);
        }
      });
      _this.sub_menu_links.on("click", function (e) {
        _this.slideLeft($(this));
        return false;
      });
      return _this.back_links.on("click", function (e) {
        _this.slideRight($(this));
        return false;
      });
    };

    FrameworkYMenu.prototype.adjustHeight = function (open_list) {
      var _this, current_height, open_list_height;
      _this = this;
      _this.root.css({
        height: "auto",
      });
      current_height = _this.root.outerHeight();
      open_list_height = open_list.outerHeight();
      if (open_list.css("position") === "absolute") {
        open_list.css("position", "relative");
        open_list_height = open_list.outerHeight();
        open_list.css("position", "absolute");
      }
      return _this.root.velocity({
        height: open_list_height,
      });
    };

    FrameworkYMenu.prototype.slideLeft = function (link) {
      var _this, sub_menu;
      _this = this;
      sub_menu = link.closest("li").find("ul").first();
      sub_menu.css({
        display: "block",
      });
      _this.adjustHeight(sub_menu);
      return sub_menu.velocity({
        translateX: ["-100%", 0],
      });
    };

    FrameworkYMenu.prototype.slideRight = function (link) {
      var _this, container, parent_container;
      _this = this;
      container = link.closest("ul");
      parent_container = container.parent().closest("ul");
      _this.adjustHeight(parent_container);
      return container.velocity({
        translateX: [0, "-100%"],
      });
    };

    return FrameworkYMenu;
  })();

  Accordion = (function () {
    function Accordion() {
      $(".accordion.headings").each(function () {
        return $(this)
          .add($(this).next(".accordion.content"))
          .wrapAll("<div class='accordion-wrapper'/>");
      });
      $(".accordion.headings li").wrapInner('<div class="trigger"></div>');
      $(".accordion.headings li .trigger").append('<div class="bg"></div>');
      $(".accordion-wrapper").each(function () {
        var accordion_content, accordion_heading;
        accordion_heading = $(this).find(".accordion.headings > li");
        accordion_content = $(this).find(".accordion.content > li");
        accordion_heading.first().addClass("active");
        accordion_content.each(function (index) {
          var content;
          content = $('<div class="content">' + $(this).html() + "</div>");
          return content.appendTo(accordion_heading.eq(index));
        });
        accordion_content.remove();
        $(this).find(".content").first().show();
        return $(this)
          .find(".trigger")
          .on("click", function () {
            var panels, this_panel;
            panels = $(this).closest(".accordion").find(".content");
            this_panel = $(this).closest("li").find(".content");
            panels.not(this_panel).slideUp(200);
            this_panel.slideDown(200);
            $(this).closest(".accordion").find("li").removeClass("active");
            return $(this).closest("li").addClass("active");
          });
      });
    }

    return Accordion;
  })();

  theme.classes.Article = (function (superClass) {
    extend(Article, superClass);

    function Article() {
      return Article.__super__.constructor.apply(this, arguments);
    }

    return Article;
  })(theme.classes.FrameworkArticle);

  theme.classes.Cart = (function (superClass) {
    extend(Cart, superClass);

    function Cart() {
      this.updateTotalsComplete = bind(this.updateTotalsComplete, this);
      return Cart.__super__.constructor.apply(this, arguments);
    }

    Cart.prototype.updateTotalsComplete = function (count) {
      if (count > 0) {
        $(".cart .number-wrapper").show();
        $(".header--mobile .number-wrapper").show();
      } else {
        $(".cart .number-wrapper").hide();
        $(".header--mobile .number-wrapper").hide();
      }
    };

    return Cart;
  })(theme.classes.FrameworkCart);

  theme.classes.FeaturedProduct = (function (superClass) {
    extend(FeaturedProduct, superClass);

    function FeaturedProduct() {
      return FeaturedProduct.__super__.constructor.apply(this, arguments);
    }

    return FeaturedProduct;
  })(theme.classes.FrameworkFeaturedProduct);

  theme.classes.FeaturedVideo = (function (superClass) {
    extend(FeaturedVideo, superClass);

    function FeaturedVideo() {
      return FeaturedVideo.__super__.constructor.apply(this, arguments);
    }

    return FeaturedVideo;
  })(theme.classes.FrameworkFeaturedVideo);

  Footer = (function () {
    function Footer(root) {
      var _this;
      this.root = root;
      this.stickyFooter = bind(this.stickyFooter, this);
      this.addListeners = bind(this.addListeners, this);
      this.load = bind(this.load, this);
      _this = this;
      _this.load();
    }

    Footer.prototype.load = function () {
      var _this;
      _this = this;
      _this.addListeners();
      return _this.stickyFooter();
    };

    Footer.prototype.addListeners = function () {
      var _this;
      _this = this;
      return WINDOW.on("resize", function () {
        return _this.stickyFooter();
      });
    };

    Footer.prototype.stickyFooter = function () {
      var total_content_height;
      total_content_height =
        HEADER.outerHeight() +
        $(".main-content").outerHeight() +
        $(".footer--root").outerHeight();
      if (WINDOW.outerHeight() > total_content_height) {
        return $(".main-content").css({
          "min-height":
            WINDOW.outerHeight() -
            $(".header--root").outerHeight() -
            $(".footer--root").outerHeight(),
        });
      }
    };

    return Footer;
  })();

  FullscreenSlider = (function () {
    function FullscreenSlider(slider_element) {
      this.eventListeners = bind(this.eventListeners, this);
      this.alignPlayButton = bind(this.alignPlayButton, this);
      this.alignCaption = bind(this.alignCaption, this);
      this.isFirstSlider = bind(this.isFirstSlider, this);
      this.getActiveIndex = bind(this.getActiveIndex, this);
      this.autoplay = bind(this.autoplay, this);
      this.removeDuplicateVideoModals = bind(
        this.removeDuplicateVideoModals,
        this
      );
      this.createSlider = bind(this.createSlider, this);
      var _this, slide_parent;
      _this = this;
      this.el = slider_element;
      slide_parent = this.el.closest(".slider");
      this.autoplay_enabled = slide_parent.data("autoplay");
      if (slide_parent.find(".slide").length < 2) {
        this.autoplay_enabled = false;
      }
      this.autoplay_frequency = slide_parent.data("rotateFrequency");
      this.transition_style = slide_parent.data("transitionStyle");
      _this.removeDuplicateVideoModals();
      this.createSlider();
      this.owl = $(".owl-carousel").data("owlCarousel");
    }

    FullscreenSlider.prototype.createSlider = function () {
      var slider, slider_options;
      slider = this;
      slider_options = {
        singleItem: true,
        navigation: false,
        paginationNumbers: false,
        scrollPerPageNav: true,
        slideSpeed: 800,
        pagination: true,
        autoHeight: true,
        autoPlay: slider.autoplay(),
        afterInit: function () {
          return slider.eventListeners();
        },
        afterAction: function () {
          slider.alignCaption();
          return slider.alignPlayButton();
        },
      };
      if (this.transition_style !== "default") {
        slider_options["transitionStyle"] = this.transition_style;
      }
      return slider.el.owlCarousel(slider_options);
    };

    FullscreenSlider.prototype.removeDuplicateVideoModals = function () {
      var _this;
      _this = this;
      return $(".modal.video:gt(0)").remove();
    };

    FullscreenSlider.prototype.autoplay = function () {
      if (this.autoplay_enabled) {
        return this.autoplay_frequency;
      }
      return false;
    };

    FullscreenSlider.prototype.getActiveIndex = function () {
      return this.el.find(".owl-pagination .owl-page.active").index();
    };

    FullscreenSlider.prototype.isFirstSlider = function () {
      var current_section_id, first_section, first_section_id;
      first_section = $(".index-sections").children("div:first");
      first_section_id = first_section.find(".slider").data("sectionId");
      current_section_id = this.el.closest(".slider").data("sectionId");
      if (first_section.hasClass("section--slideshow")) {
        return current_section_id === first_section_id;
      }
      return false;
    };

    FullscreenSlider.prototype.alignCaption = function () {
      var caption,
        caption_height,
        caption_width,
        slide,
        slide_padding,
        top_offset;
      slide = this.el.find(".owl-item").eq(this.getActiveIndex());
      caption = slide.find(".caption");
      caption.css("visibility", "hidden");
      caption_height = caption.outerHeight();
      caption_width = caption.outerWidth();
      slide_padding = 30;
      if (
        theme.utils.transparentMenuIsEnabled() &&
        this.isFirstSlider() === true
      ) {
        top_offset = $(".header--root").outerHeight();
      } else {
        top_offset = 0;
      }
      return theme.utils.imagesLoaded(slide.find("img").first(), function () {
        var left_offset, middle_top, slide_height, slide_width;
        slide_height = slide.outerHeight();
        slide_width = slide.outerWidth();
        if (caption.hasClass("top")) {
          caption.css("top", top_offset + slide_padding);
        } else if (caption.hasClass("middle")) {
          middle_top =
            top_offset + (slide_height - top_offset - caption_height) / 2;
          caption.css("top", middle_top);
        }
        if (caption.hasClass("center")) {
          left_offset = (slide_width - caption_width) / 2;
          caption.css("left", left_offset);
        }
        return caption.css("visibility", "visible");
      });
    };

    FullscreenSlider.prototype.alignPlayButton = function () {
      var play_button, slide;
      slide = this.el.find(".owl-item").eq(this.getActiveIndex());
      play_button = slide.find(".play-button");
      play_button.css("visibility", "hidden");
      if (
        theme.utils.transparentMenuIsEnabled() &&
        $(".header--root").css("position") === "absolute"
      ) {
        theme.utils.imagesLoaded(slide.find("img").first(), function () {
          var play_button_height, slide_height, video_offset;
          slide_height = slide.outerHeight();
          play_button_height = play_button.outerHeight();
          video_offset = (slide_height - play_button_height) / 2;
          return play_button.css({
            "margin-top": 0,
            top: video_offset,
          });
        });
      } else {
        play_button.css({
          "margin-top": "-40px",
          top: "50%",
        });
      }
      return play_button.css("visibility", "visible");
    };

    FullscreenSlider.prototype.eventListeners = function () {
      var slider;
      slider = this;
      this.el.find(".play-button").on("click", function () {
        var video_modal;
        video_modal = new VideoModal($(this).closest(".video"));
        video_modal.open();
        slider.owl.stop();
        return false;
      });
      this.el.find(".owl-pagination .owl-page").on("click", function () {
        return slider.owl.stop();
      });
      return DOC.on("header.transparentMenuIsUpdated", slider.alignCaption);
    };

    return FullscreenSlider;
  })();

  theme.classes.Header = (function () {
    function Header(root) {
      var _this;
      this.root = root;
      this.moveYMenu = bind(this.moveYMenu, this);
      this.sectionListeners = bind(this.sectionListeners, this);
      this.load = bind(this.load, this);
      _this = this;
      _this.load();
    }

    Header.prototype.load = function () {
      var _this;
      _this = this;
      _this.searchAndAccount();
      _this.sectionListeners();
      return _this.moveYMenu();
    };

    Header.prototype.sectionListeners = function () {
      var _this;
      _this = this;
      return _this.root.on("theme:section:load", function () {
        theme.partials.FrameworkOffCanvas.unload();
        return theme.partials.FrameworkOffCanvas.load();
      });
    };

    Header.prototype.moveYMenu = function () {
      var _this;
      _this = this;
      $(".mobile-nav--menu").empty();
      return $(".y-menu").appendTo(".mobile-nav--menu");
    };

    Header.prototype.searchAndAccount = function () {
      var _this;
      _this = this;
      $(".account-open").on("click", function () {
        $(this)
          .closest(".menu")
          .fadeOut(100, function () {
            return $(".account-container").fadeIn(200);
          });
        return false;
      });
      return $(".account-close").on("click", function () {
        $(".account-container").fadeOut(100, function () {
          return $(".search-account .menu").fadeIn(200);
        });
        return false;
      });
    };

    return Header;
  })();

  HorizontalTabs = (function () {
    function HorizontalTabs() {
      $(".tabs-horizontal.headings").each(function () {
        return $(this)
          .add($(this).next(".tabs.content"))
          .wrapAll("<div class='tabs-wrapper horizontal'/>");
      });
      $(".tabs-horizontal.headings li").wrapInner(
        '<div class="trigger"></div>'
      );
      $(".tabs-horizontal.headings li .trigger").append(
        '<div class="bg"></div>'
      );
      $(".tabs-wrapper.horizontal").each(function () {
        var tab_content, tab_headings;
        tab_headings = $(this).find(".headings > li");
        tab_content = $(this).find(".tabs.content > li");
        tab_content.first().addClass("active");
        tab_headings.first().addClass("active");
        return tab_headings.on("click", function () {
          tab_headings.removeClass("active");
          tab_content.removeClass("active");
          $(this).addClass("active");
          return tab_content.eq($(this).index()).addClass("active");
        });
      });
    }

    return HorizontalTabs;
  })();

  MediaQueries = (function () {
    function MediaQueries() {
      this.load = bind(this.load, this);
      var _this;
      _this = this;
      _this.listeners();
      _this.load();
    }

    MediaQueries.prototype.load = function () {
      var _this, current_window;
      _this = this;
      if (
        window.matchMedia("only screen and (min-width: " + mq_medium + "px)")
          .matches
      ) {
        if (current_window !== "large") {
          return (current_window = "large");
        }
      } else if (
        window.matchMedia("only screen and (min-width: " + mq_small + "px)")
          .matches
      ) {
        if (current_window !== "medium") {
          return (current_window = "medium");
        }
      } else {
        if (current_window !== "small") {
          return (current_window = "small");
        }
      }
    };

    MediaQueries.prototype.listeners = function () {
      var _this;
      _this = this;
      return WINDOW.on("resize.section-" + _this.section_id, _this.load);
    };

    return MediaQueries;
  })();

  Popup = (function () {
    function Popup(clazz) {
      var checkContentOverflow,
        closeDialog,
        eventListeners,
        fillPopupDialog,
        fillSizeChartDialog,
        getCustomClasses,
        getImage,
        getNewsletter,
        getPageContent,
        getSocialIcons,
        getWrapper,
        hideMask,
        ieCenter,
        isExpired,
        mask,
        maskIsActive,
        modal,
        modal_clazz,
        openDialog,
        passesExpiration,
        resetExpiration,
        showMask,
        storeExpiration;
      this.clazz = clazz;
      modal = null;
      modal_clazz = this.clazz;
      mask = $(".popup-modal-mask");
      storeExpiration = function () {
        var date, e, expires, object, seconds_from_now;
        date = new Date();
        seconds_from_now = 1000 * 60 * 60 * 24 * popup_config.days_until;
        expires = date.setTime(date.getTime() + seconds_from_now);
        object = {
          expires: expires,
        };
        try {
          return (localStorage[popup_config.storage_key] = JSON.stringify(
            object
          ));
        } catch (error1) {
          e = error1;
          return false;
        }
      };
      resetExpiration = function () {
        localStorage.removeItem(popup_config.storage_key);
        return storeExpiration();
      };
      isExpired = function () {
        var expires, now, object;
        object = JSON.parse(localStorage[popup_config.storage_key]);
        expires = object.expires;
        now = new Date().getTime();
        if (parseFloat(expires - now) <= 0) {
          resetExpiration();
          return true;
        }
        return false;
      };
      passesExpiration = function () {
        var passed;
        passed = false;
        if (Storage === "undefined" || popup_config.test_mode) {
          passed = true;
        } else if (
          typeof localStorage[popup_config.storage_key] === "undefined"
        ) {
          passed = true;
          storeExpiration();
        } else {
          passed = isExpired();
        }
        return passed;
      };
      maskIsActive = function () {
        return (
          $(".popup-modal").is(":visible") ||
          ($(".modal-mask").length > 0 && $(".modal-mask").is(":visible"))
        );
      };
      showMask = function () {
        mask.show();
        return PAGE.addClass("modal-on");
      };
      hideMask = function () {
        mask.hide();
        return PAGE.removeClass("modal-on");
      };
      getImage = function () {
        if (popup_config.show_image_enabled === false) {
          return "";
        }
        if (popup_config.image_link.length > 0) {
          return $(
            '<div class="popup-image"> <a href="' +
              popup_config.image_link +
              '">' +
              popup_config.show_image_url +
              "</a> </div>"
          );
        } else {
          return $('<div class="popup-image">').append(
            popup_config.show_image_url
          );
        }
      };
      getNewsletter = function () {
        var subscribe_module;
        if (popup_config.newsletter_enabled === false) {
          return "";
        }
        subscribe_module = $("<div id='subscribe_module'></div>").append(
          $(".popup--root")
        );
        return subscribe_module;
      };
      getSocialIcons = function () {
        if (popup_config.social_icons_enabled === false) {
          return "";
        }
        return $(".social-follow").clone();
      };
      getCustomClasses = function () {
        var class_list;
        class_list = "";
        class_list +=
          popup_config.show_image_enabled === true
            ? " has-image"
            : " has-no-image";
        class_list +=
          popup_config.page_content.length > 0
            ? " has-page-content"
            : " has-no-page-content";
        class_list += popup_config.newsletter_enabled
          ? " has-newsletter"
          : " has-no-newsletter";
        class_list += popup_config.social_icons_enabled
          ? " has-social-icons"
          : " has-no-social-icons";
        return class_list;
      };
      getWrapper = function () {
        return (
          '<dialog class="' +
          modal_clazz +
          " popup-modal" +
          getCustomClasses() +
          '" />'
        );
      };
      getPageContent = function () {
        var page_content;
        page_content = null;
        if (popup_config.page_content.length < 1) {
          return "";
        }
        $.getJSON("/pages/" + popup_config.page_content + ".json", function (
          data,
          textStatus
        ) {
          return (page_content =
            "<div class='page-contents'>" + data.page.body_html + "</div>");
        });
        return page_content;
      };
      fillSizeChartDialog = function () {
        var dialog;
        if ($(".popup-modal.size-chart").length < 1) {
          dialog = {
            wrapper: getWrapper(),
          };
          PAGE.append($(dialog.wrapper).append($(".size-chart")));
        }
        return openDialog();
      };
      fillPopupDialog = function () {
        var dialog, getInnerContent, render;
        dialog = {
          wrapper: getWrapper(),
          newsletter: getNewsletter(),
          social_icons: getSocialIcons(),
          image: getImage(),
        };
        getInnerContent = function () {
          if (
            popup_config.page_content.length < 1 &&
            popup_config.newsletter_enabled === false &&
            popup_config.social_icons_enabled === false
          ) {
            return "";
          }
          return $("<div class='inner' />").append(
            dialog.body,
            dialog.newsletter,
            dialog.social_icons
          );
        };
        render = function () {
          PAGE.append(
            $(dialog.wrapper).append(dialog.image, getInnerContent())
          );
          return openDialog();
        };
        if (popup_config.page_content.length > 0) {
          return $.getJSON(
            "/pages/" + popup_config.page_content + ".json",
            function (data, textStatus) {
              dialog["body"] =
                "<div class='page-contents'>" + data.page.body_html + "</div>";
              return render();
            }
          );
        } else {
          return render();
        }
      };
      checkContentOverflow = function () {
        return setTimeout(function () {
          if (modal.length > 0 && $(".popup-modal-mask").is(":visible")) {
            return theme.utils.imagesLoaded(modal, function () {
              var dialog_height;
              dialog_height = $(
                'dialog[class*="' + modal_clazz + '"]:last-of-type'
              ).outerHeight();
              if (dialog_height >= WINDOW.height()) {
                return PAGE.addClass("modal-unfix");
              } else {
                return PAGE.removeClass("modal-unfix");
              }
            });
          }
        }, 0);
      };
      ieCenter = function () {
        return modal.css({
          marginTop: -(modal.outerHeight() * 0.5) + "px",
          marginLeft: -(modal.outerWidth() * 0.5) + "px",
        });
      };
      openDialog = function () {
        modal = $('dialog[class*="' + modal_clazz + '"]');
        eventListeners();
        modal.addClass("opened").removeClass("closed");
        checkContentOverflow();
        return showMask();
      };
      eventListeners = function () {
        var removeAnimation;
        removeAnimation = function (event) {
          if (event.originalEvent.animationName === "modal-close") {
            if (modal_clazz === "popup") {
              return modal.remove();
            } else {
              return modal.removeClass("closed, completed");
            }
          } else {
            return modal.addClass("completed").removeClass("opened");
          }
        };
        DOC.on("click", ".popup-modal", function (e) {
          e.stopPropagation();
          if (e.target === this) {
            return closeDialog();
          }
        });
        $(".popup-modal-close").on("click", closeDialog);
        mask.on("click", closeDialog);
        modal.on(
          "animationend webkitAnimationEnd MSAnimationEnd oAnimationEnd",
          removeAnimation
        );
        WINDOW.on("resize", checkContentOverflow);
        DOC.on("keydown", function (e) {
          if (modal.hasClass("completed") && e.keyCode === 27) {
            return closeDialog();
          }
        });
        return $("dialog.popup-modal #contact_form").on("submit", function (
          event
        ) {
          var form;
          form = this;
          modal = $(this).closest(".popup-modal");
          modal.find(".error, .success").remove();
          event.preventDefault();
          if (modal.find('input[type="email"]').val().length === 0) {
            modal
              .find(".inner")
              .prepend(
                '<p class="error">' +
                  theme.translation.newsletter_email_blank +
                  "</p>"
              );
            return false;
          } else {
            form.submit();
          }
          return false;
        });
      };
      closeDialog = function () {
        modal.addClass("closed").removeClass("completed").removeClass("opened");
        return hideMask();
      };
      if (modal_clazz !== "popup") {
        fillSizeChartDialog();
      } else {
        if (
          popup_config.enabled &&
          passesExpiration() &&
          (popup_config.page_content.length > 0 ||
            popup_config.newsletter_enabled ||
            popup_config.show_image_enabled ||
            popup_config.social_icons_enabled)
        ) {
          setTimeout(function () {
            if (
              !(maskIsActive() === true || $(".popup-modal-mask").length < 1)
            ) {
              return fillPopupDialog();
            }
          }, popup_config.seconds_until * 1000);
        }
      }
    }

    return Popup;
  })();

  theme.classes.ProductGrid = (function () {
    function ProductGrid(root) {
      var _this;
      this.root = root;
      this.hoverImagesLoaded = bind(this.hoverImagesLoaded, this);
      this.listeners = bind(this.listeners, this);
      this.matchImageHeights = bind(this.matchImageHeights, this);
      this.detectOnboarding = bind(this.detectOnboarding, this);
      this.load = bind(this.load, this);
      _this = this;
      _this.items = _this.root.find(".product--root");
      _this.onboarding = _this.detectOnboarding();
      _this.in_slider = _this.root.data("in-slider");
      _this.load();
    }

    ProductGrid.prototype.load = function () {
      var _this;
      _this = this;
      _this.listeners();
      _this.hoverImagesLoaded();
      if (!_this.in_slider) {
        return _this.matchImageHeights();
      }
    };

    ProductGrid.prototype.detectOnboarding = function () {
      var _this;
      _this = this;
      if (_this.items.first().find(".placeholder").length) {
        return true;
      }
      return false;
    };

    ProductGrid.prototype.matchImageHeights = function () {
      var _this;
      _this = this;
      return theme.utils.matchImageHeights(
        _this.root,
        _this.items,
        ".product--image-wrapper"
      );
    };

    ProductGrid.prototype.listeners = function () {
      var _this;
      _this = this;
      if (!_this.in_slider) {
        return $(window).on(
          "resize.ProductGrid",
          theme.utils.debounce(100, function () {
            return _this.matchImageHeights();
          })
        );
      }
    };

    ProductGrid.prototype.hoverImagesLoaded = function () {
      var _this;
      _this = this;
      return _this.items.filter('[data-hover-image="true"]').each(function () {
        var product;
        product = $(this);
        return theme.utils.imagesLoaded(product, function () {
          return product.attr("data-hover-image", "loaded");
        });
      });
    };

    return ProductGrid;
  })();

  theme.classes.ProductRecommendations = (function (superClass) {
    extend(ProductRecommendations, superClass);

    function ProductRecommendations() {
      return ProductRecommendations.__super__.constructor.apply(
        this,
        arguments
      );
    }

    return ProductRecommendations;
  })(theme.classes.FrameworkProductRecommendations);

  ProductSlider = (function () {
    function ProductSlider(root) {
      var _this;
      this.root = root;
      this.listeners = bind(this.listeners, this);
      this.afterUpdate = bind(this.afterUpdate, this);
      this.load = bind(this.load, this);
      _this = this;
      _this.product_grid = _this.root.find(".product-grid--root");
      _this.product_item = _this.root.find(".product--root");
      _this.load();
      _this.listeners();
    }

    ProductSlider.prototype.load = function () {
      var _this;
      _this = this;
      _this.product_grid.owlCarousel({
        items: 4,
        navigation: true,
        scrollPerPage: true,
        slideSpeed: 800,
        lazyLoad: false,
        pagination: false,
        navigationText: false,
        afterUpdate: _this.afterUpdate(),
      });
      return _this.product_item.show();
    };

    ProductSlider.prototype.afterUpdate = function () {
      var _this;
      _this = this;
      return $(window).trigger("ProductSlider.afterUpdate");
    };

    ProductSlider.prototype.listeners = function () {
      var _this;
      _this = this;
      return $(window).on(
        "resize.ProductGrid",
        theme.utils.debounce(100, function () {
          return _this.loadCarousel;
        })
      );
    };

    return ProductSlider;
  })();

  VerticalTabs = (function () {
    function VerticalTabs() {
      $(".tabs-vertical.headings").each(function () {
        return $(this)
          .add($(this).next(".tabs.content"))
          .wrapAll("<div class='tabs-wrapper vertical'/>");
      });
      $(".tabs-vertical.headings li").wrapInner('<div class="trigger"></div>');
      $(".tabs-vertical.headings li .trigger").append('<div class="bg"></div>');
      $(".tabs-wrapper.vertical").each(function () {
        var tab_content, tab_headings, tab_wrapper;
        tab_wrapper = $(this);
        tab_headings = $(this).find(".headings > li");
        tab_content = $(this).find(".tabs.content > li");
        tab_content.first().addClass("active");
        tab_headings.first().addClass("active");
        return tab_headings.on("click", function () {
          tab_headings.removeClass("active");
          tab_content.removeClass("active");
          $(this).addClass("active");
          tab_content.eq($(this).index()).addClass("active");
          if (general_scroll_to_active_item) {
            if (
              matchMedia("only screen and (min-width: " + mq_small + "px)")
                .matches
            ) {
              return $("html, body").animate(
                {
                  scrollTop: tab_wrapper.offset().top - 50,
                },
                "slow"
              );
            } else {
              return $("html, body").animate(
                {
                  scrollTop:
                    tab_wrapper.offset().top +
                    tab_wrapper.find(".headings").outerHeight() -
                    50,
                },
                "slow"
              );
            }
          }
        });
      });
    }

    return VerticalTabs;
  })();

  VideoModal = (function () {
    function VideoModal(video) {
      this.createIframe = bind(this.createIframe, this);
      this.extractVideoId = bind(this.extractVideoId, this);
      this.extractVideoType = bind(this.extractVideoType, this);
      this.eventListeners = bind(this.eventListeners, this);
      this.centerPosition = bind(this.centerPosition, this);
      this.close = bind(this.close, this);
      this.open = bind(this.open, this);
      this.opened = false;
      this.video = video;
      this.modal = $(".video.modal");
      this.player_button = video.find(".player-button");
      this.src_url = video.find(".play-button").attr("href");
      this.type = this.extractVideoType();
      this.id = this.extractVideoId();
      this.iframe = this.createIframe();
      this.caption = video.find(".caption");
    }

    VideoModal.prototype.open = function () {
      this.opened = true;
      this.modal.find(".flex-video").append(this.iframe);
      if (this.caption.length > 0) {
        this.modal.find(".caption").append(this.caption.html());
        this.modal.addClass("wide");
      } else {
        this.modal.find(".player").removeClass("large-8");
        this.modal.find(".caption").hide();
        this.modal.removeClass("wide");
      }
      this.player_button.hide();
      $(".modal-mask").show();
      this.modal.find(".close").show();
      this.modal.fadeIn();
      this.centerPosition();
      $(".modal").fadeIn(0);
      return this.eventListeners();
    };

    VideoModal.prototype.close = function () {
      this.opened = false;
      this.modal.find(".flex-video").empty();
      this.modal.find(".caption").empty();
      this.modal.hide();
      $(".modal-mask").fadeOut();
      if (this.caption.length === 0) {
        this.modal.find(".player").addClass("large-8");
        return this.modal.find(".caption").show();
      }
    };

    VideoModal.prototype.centerPosition = function () {
      if (WINDOW.height() < this.modal.outerHeight()) {
        return this.modal.css({
          position: "absolute",
          top: "30px",
          "margin-top": 0,
          "margin-left": -(this.modal.outerWidth() / 2),
        });
      } else {
        return this.modal.css({
          position: "fixed",
          top: "50%",
          "margin-top": -(this.modal.outerHeight() / 2),
          "margin-left": -(this.modal.outerWidth() / 2),
        });
      }
    };

    VideoModal.prototype.eventListeners = function () {
      var modal;
      modal = this;
      this.modal.find(".close").on("click", function () {
        return modal.close();
      });
      WINDOW.on("resize", function () {
        return modal.centerPosition();
      });
      DOC.on("keydown", function (e) {
        if (modal.opened) {
          if (e.keyCode === 27) {
            return modal.close();
          }
        }
      });
      $(".modal-mask").on("click", function () {
        return modal.close();
      });
      return this.player_button.on("click", function () {
        return false;
      });
    };

    VideoModal.prototype.extractVideoType = function () {
      var matches, re;
      re = /\/\/(?:www\.)?youtu(?:\.be|be\.com)\/(?:watch\?v=|embed\/)?([a-z0-9_\-]+)/i;
      matches = re.exec(this.src_url);
      if (matches) {
        return "youtube";
      } else {
        re = /^.*(vimeo)\.com\/(?:watch\?v=)?(.*?)(?:\z|$|&)/;
        matches = re.exec(this.src_url);
        if (matches) {
          return "vimeo";
        }
      }
      return false;
    };

    VideoModal.prototype.extractVideoId = function () {
      var match, regExp;
      if (this.type === "youtube") {
        regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        match = this.src_url.match(regExp);
        if (match && match[2].length === 11) {
          return match[2];
        }
      } else if (this.type === "vimeo") {
        regExp = /^.*(vimeo)\.com\/(?:watch\?v=)?(.*?)(?:\z|$|&)/;
        match = this.src_url.match(regExp);
        if (match) {
          return match[2];
        }
      }
    };

    VideoModal.prototype.createIframe = function () {
      if (this.type === "youtube") {
        return (
          '<iframe  src="//www.youtube.com/embed/' +
          this.id +
          '?autoplay=1&rel=0" frameborder="0" allowfullscreen></iframe>'
        );
      } else if (this.type === "vimeo") {
        return (
          '<iframe src="//player.vimeo.com/video/' +
          this.id +
          '?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff&amp;autoplay=1?" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'
        );
      }
    };

    return VideoModal;
  })();

  theme.classes.XMenu = (function (superClass) {
    extend(XMenu, superClass);

    function XMenu(root) {
      var _this;
      this.root = root;
      this.slideDown = bind(this.slideDown, this);
      this.hideHeader = bind(this.hideHeader, this);
      this.showHeader = bind(this.showHeader, this);
      this.headerListeners = bind(this.headerListeners, this);
      this.isHeaderTransparent = bind(this.isHeaderTransparent, this);
      _this = this;
      _this.header = $(".header--root");
      _this.header_bg = _this.header.find(".header--bg");
      _this.header_timer = null;
      _this.transparent_header = _this.isHeaderTransparent();
      XMenu.__super__.constructor.apply(this, arguments);
      _this.headerListeners();
    }

    XMenu.prototype.isHeaderTransparent = function () {
      var _this;
      _this = this;
      if ($(".header--root").attr("data-header--menu-transparent") === "true") {
        return true;
      } else {
        return false;
      }
    };

    XMenu.prototype.headerListeners = function () {
      var _this;
      _this = this;
      _this.parents_with_sub_menu.on("mouseenter", function (e) {
        return clearTimeout(_this.header_timer);
      });
      _this.header.on("mouseenter touchstart", function (e) {
        clearTimeout(_this.header_timer);
        return _this.showHeader();
      });
      return _this.header.on("mouseleave", function (e) {
        return (_this.header_timer = setTimeout(function () {
          return _this.hideHeader();
        }, 500));
      });
    };

    XMenu.prototype.showHeader = function () {
      var _this;
      _this = this;
      if (parseInt(_this.header_bg.css("opacity")) !== 0) {
        return;
      }
      return _this.header_bg.velocity(
        {
          opacity: [0.95],
        },
        {
          duration: 400,
          queue: false,
        }
      );
    };

    XMenu.prototype.hideHeader = function () {
      var _this;
      _this = this;
      if (
        _this.parents_with_sub_menu.filter('[data-main-menu--open="true"]')
          .length > 0
      ) {
        return false;
      }
      _this.header_bg.velocity(
        {
          opacity: 0,
        },
        {
          duration: 400,
        }
      );
      return _this.slideUp();
    };

    XMenu.prototype.slideDown = function (link, delay) {
      var _this, opacity;
      if (delay == null) {
        delay = false;
      }
      _this = this;
      if (delay !== "complete") {
        opacity = Math.round(parseFloat(_this.header_bg.css("opacity")) * 100);
        if (opacity < 90 && _this.transparent_header) {
          delay = 400;
        }
      }
      return XMenu.__super__.slideDown.call(this, link, delay);
    };

    return XMenu;
  })(theme.classes.FrameworkXMenu);

  theme.classes.Blog = (function (superClass) {
    extend(Blog, superClass);

    function Blog() {
      return Blog.__super__.constructor.apply(this, arguments);
    }

    return Blog;
  })(theme.classes.FrameworkBlog);

  Collection = (function () {
    function Collection() {
      DOC.on("change", ".tags-listbox", function () {
        var collFilters, newTags, query;
        newTags = [];
        collFilters = $(this);
        collFilters.each(function (index, element) {
          if ($(this).val()) {
            return newTags.push($(this).val());
          }
        });
        if (newTags.length) {
          query = newTags.join("+");
          return (window.location.href = $(theme.collection.tag)
            .attr("href")
            .replace("tag", query));
        } else {
          if (theme.collection.handle) {
            return (window.location.href =
              "/collections/" + theme.collection.handle);
          } else if (
            theme.collection.products.first_type === theme.collection.title
          ) {
            return (window.location.href = theme.collection.url_for_type);
          } else if (
            theme.collection.products.first_vendor === theme.collection.title
          ) {
            return (window.location.href = theme.collection.url_for_vendor);
          }
        }
      });
    }

    return Collection;
  })();

  theme.classes.ProductPage = (function () {
    function ProductPage(root) {
      var _this;
      this.root = root;
      this.cssGridFallback = bind(this.cssGridFallback, this);
      this.modalCloseListener = bind(this.modalCloseListener, this);
      this.resizeListeners = bind(this.resizeListeners, this);
      this.imageZoom = bind(this.imageZoom, this);
      this.getActiveImageId = bind(this.getActiveImageId, this);
      this.updateVariantImage = bind(this.updateVariantImage, this);
      this.thumbListener = bind(this.thumbListener, this);
      this.variantSelected = bind(this.variantSelected, this);
      this.sizeChart = bind(this.sizeChart, this);
      this.updateLowInStock = bind(this.updateLowInStock, this);
      this.getVariantQuantity = bind(this.getVariantQuantity, this);
      this.getLowInStockAmount = bind(this.getLowInStockAmount, this);
      this.getLowInStockMessage = bind(this.getLowInStockMessage, this);
      this.mobileModalHandler = bind(this.mobileModalHandler, this);
      this.addProductComplete = bind(this.addProductComplete, this);
      this.addToCartListener = bind(this.addToCartListener, this);
      this.load = bind(this.load, this);
      _this = this;
      _this.thumbs = _this.root.find(".product-page--thumb");
      _this.main_images = _this.root.find(".product-page--image");
      _this.zoom_images = _this.root.find(".product-page--zoom-image");
      _this.image_container = _this.root.find(
        ".product-page--images-container"
      );
      _this.magnify = parseFloat(_this.root.data("magnify"));
      _this.size_chart_enabled = _this.root.data("size-chart-enabled");
      _this.size_chart_option = _this.root
        .data("size-chart-option")
        .toLowerCase();
      _this.form = _this.root.find(".product-page--cart-form form");
      _this.add_button = _this.root.find("#add");
      _this.load();
    }

    ProductPage.prototype.load = function () {
      var _this, photos;
      _this = this;
      _this.cssGridFallback();
      _this.thumbListener();
      _this.imageZoom();
      _this.resizeListeners();
      _this.modalCloseListener();
      _this.mobileModalHandler();
      if (theme.settings.cart_type === "drawer") {
        _this.addToCartListener();
      }
      _this.root.on("theme:section:load", function () {
        new Cart();
        new Accordion();
        new HorizontalTabs();
        new VerticalTabs();
        return new Shopify.OptionSelectors("variant-listbox", {
          product: product_json,
          onVariantSelected: _this.variantSelected,
          enableHistoryState: true,
        });
      });
      photos = $("article .photos");
      DOC.on("click", ".size-chart-trigger", function (e) {
        e.preventDefault();
        return new Popup("size-chart-container");
      });
      photos.on("click", function () {
        return false;
      });
      $("article .custom.dropdown").hide();
      if (product_options_size === 1 && product_options_first !== "Title") {
        return $(".selector-wrapper:eq(0)").prepend(
          "<label>" + product_options_first + "</label>"
        );
      }
    };

    ProductPage.prototype.addToCartListener = function () {
      var _this;
      _this = this;
      if (_this.form.length > 0 && theme.settings.cart_type === "drawer") {
        return _this.form.on("submit", function () {
          _this.form.find("p.error").remove();
          _this.add_button.attr("data-loading", "true");
          theme.partials.Cart.addItem($(this), function (success, error) {
            if (success) {
              return theme.partials.Cart.updateAllHtml(function () {
                return _this.addProductComplete();
              });
            } else {
              _this.form.append('<p class="error">' + error + "</p>");
              return _this.add_button.attr("data-loading", "false");
            }
          });
          return false;
        });
      }
    };

    ProductPage.prototype.addProductComplete = function () {
      var _this;
      _this = this;
      _this.add_button.attr("data-loading", "false");
      return $('[data-off-canvas--open="right-sidebar"]')
        .first()
        .trigger("click");
    };

    ProductPage.prototype.mobileModalHandler = function () {
      var _this, modal_links;
      _this = this;
      modal_links = _this.image_container.find(".modal--link");
      return modal_links.on("click", function (e) {
        if (theme.utils.mqs.current_window === "small") {
          return e.stopImmediatePropagation();
        }
      });
    };

    ProductPage.prototype.getLowInStockMessage = function (quantity) {
      var _this, message;
      _this = this;
      message =
        quantity === 1
          ? theme.products.low_in_stock.one
          : theme.products.low_in_stock.other;
      if (quantity > 1) {
        message = message.replace(/\d+/, quantity);
      }
      return message;
    };

    ProductPage.prototype.getLowInStockAmount = function () {
      var _this;
      _this = this;
      return _this.root.data("product-low-in-stock-amount");
    };

    ProductPage.prototype.getVariantQuantity = function (variant) {
      var option;
      option = $("#variant-listbox").find('[value="' + variant.id + '"]');
      if (option.length > 0) {
        return parseInt(option.data("inventory-quantity"));
      } else {
        return "not available";
      }
    };

    ProductPage.prototype.updateLowInStock = function (variant) {
      var _this, quantity, threshold;
      _this = this;
      if (variant === null) {
        return false;
      }
      quantity = _this.getVariantQuantity(variant);
      threshold = _this.getLowInStockAmount();
      if (threshold === "undefined") {
        return false;
      }
      if (quantity > 0 && quantity <= threshold) {
        $(".product-low-in-stock")
          .html("<p>" + _this.getLowInStockMessage(quantity) + "</p>")
          .show();
      } else {
        $(".product-low-in-stock").hide();
      }
      return DOC.on(
        ".shopify-section section--products shopify:section:select",
        function () {
          return _this.updateLowInStock(variant);
        }
      );
    };

    ProductPage.prototype.sizeChart = function (product_options) {
      var _this;
      _this = this;
      if (!_this.size_chart_enabled) {
        return;
      }
      return $(".selector-wrapper").each(function (index, element) {
        if (
          product_options[index].toLowerCase() === _this.size_chart_option &&
          $(element).find(".size-chart-trigger").length === 0
        ) {
          return $(element).append(
            '<div class="font--block-link"><a class="size-chart-trigger" href="#">' +
              theme.products.size_chart.label +
              "</a></div>"
          );
        }
      });
    };

    ProductPage.prototype.variantSelected = function (
      variant,
      selector,
      options
    ) {
      var _this, actual_price, compare_price, show_low_in_stock;
      _this = this;
      $(".compare-price").html("");
      $('.product-unavailable [type="submit"]').prop("disabled", true);
      if (variant && variant.available) {
        $(".quanity-cart-row, .product-page--smart-payment-buttons").show();
        $(".product-unavailable").hide();
      } else {
        $(".quanity-cart-row, .product-page--smart-payment-buttons").hide();
        $(".product-unavailable").show();
        $(".product-low-in-stock").hide();
        $('.product-unavailable [type="submit"]').prop("disabled", false);
        if (product_variant_size > 1 && variant) {
          $(".product-unavailable form .email-body").attr(
            "value",
            "Please notify me when this is back in stock: " +
              product_title +
              " - " +
              variant.title
          );
        }
      }
      if (variant) {
        actual_price = Shopify.formatMoney(variant.price, shop_money_format);
        $(".actual-price").replaceWith(
          '<span class="actual-price money font--accent" itemprop="price">' +
            actual_price +
            "</span>"
        );
        if (variant.compare_at_price > variant.price) {
          compare_price = Shopify.formatMoney(
            variant.compare_at_price,
            shop_money_format
          );
          $(".compare-price").html(
            '<span class="money font--light-accent">' +
              compare_price +
              "</span>"
          );
        }
        if (currency_switcher_enabled) {
          Currency.convertAll(shopCurrency, $("[name=currencies]").val());
        }
        if (variant.featured_image) {
          _this.updateVariantImage(variant.featured_image.id);
        }
      }
      show_low_in_stock = _this.getLowInStockAmount() === false ? false : true;
      if (show_low_in_stock === true) {
        _this.updateLowInStock(variant);
      }
      _this.sizeChart(selector.product.options);
    };

    ProductPage.prototype.thumbListener = function () {
      var _this;
      _this = this;
      return _this.thumbs.on("keypress click", function (e) {
        var variant_id;
        if (theme.utils.a11yClick(e)) {
          variant_id = $(this).data("id");
          _this.updateVariantImage(variant_id);
        }
        return _this.imageZoom();
      });
    };

    ProductPage.prototype.updateVariantImage = function (variant_id) {
      var _this, variant_image, variant_thumb;
      _this = this;
      _this.main_images.attr("data-active", "false");
      variant_image = _this.main_images.filter(
        '[data-id="' + variant_id + '"]'
      );
      variant_image.attr("data-active", "true");
      _this.thumbs.attr("data-active", "false");
      variant_thumb = _this.thumbs.filter('[data-id="' + variant_id + '"]');
      variant_thumb.attr("data-active", "true");
      return _this.imageZoom();
    };

    ProductPage.prototype.getActiveImageId = function () {
      var _this, active_image, active_image_id;
      _this = this;
      active_image = _this.main_images.filter('[data-active="true"]');
      active_image_id = active_image.data("id");
      return active_image_id;
    };

    ProductPage.prototype.imageZoom = function () {
      var _this,
        active_zoom_image,
        aspect_ratio,
        left_position,
        top_position,
        wrapper_height,
        wrapper_width,
        x_ratio,
        y_ratio;
      _this = this;
      if (
        _this.root.data("zoom-enabled") === false ||
        _this.main_images.length < 1
      ) {
        return;
      }
      _this.image_container.off("mouseenter.ProductPage.imageZoom");
      _this.image_container.off("mouseleave.ProductPage.imageZoom");
      active_zoom_image = _this.zoom_images.filter(
        '[data-id="' + _this.getActiveImageId() + '"]'
      );
      aspect_ratio = active_zoom_image.data("aspect-ratio");
      wrapper_width = _this.image_container.width();
      wrapper_height = _this.image_container.height();
      active_zoom_image.css("width", wrapper_width * _this.magnify);
      active_zoom_image
        .find(".image--root")
        .css("width", wrapper_width * _this.magnify);
      active_zoom_image.find("img").addClass("lazypreload");
      top_position = _this.image_container.offset().top;
      left_position = _this.image_container.offset().left;
      x_ratio = (wrapper_width * _this.magnify - wrapper_width) / wrapper_width;
      y_ratio =
        (wrapper_height * _this.magnify - wrapper_height) / wrapper_height;
      _this.image_container.on("mouseenter.ProductPage.imageZoom", function () {
        return active_zoom_image.css("display", "block");
      });
      _this.image_container.on("mouseleave.ProductPage.imageZoom", function () {
        return active_zoom_image.css("display", "none");
      });
      return _this.image_container.on("mousemove", function (e) {
        var relative_left, relative_top;
        relative_left = e.pageX - left_position;
        relative_top = e.pageY - top_position;
        return active_zoom_image.css({
          left: relative_left * -x_ratio,
          top: relative_top * -y_ratio,
        });
      });
    };

    ProductPage.prototype.resizeListeners = function () {
      var _this;
      _this = this;
      return $(window).on(
        "resize.ProductPage",
        theme.utils.debounce(100, function () {
          return _this.imageZoom();
        })
      );
    };

    ProductPage.prototype.modalCloseListener = function () {
      var _this;
      _this = this;
      return $(window).on("FrameworkModal.afterClose", function () {
        return _this.imageZoom();
      });
    };

    ProductPage.prototype.cssGridFallback = function () {
      var _this;
      _this = this;
      if (!Modernizr.cssgrid) {
        $(
          ".product-page--title-n-vendor, .product-page--cart-form-block, .product-page--description"
        ).wrapAll('<div class="product-page--no-grid--left-column"></div>');
        return $(".product-page--images").wrapAll(
          '<div class="product-page--no-grid--right-column"></div>'
        );
      }
    };

    return ProductPage;
  })();

  // Crabtree Bits

  theme.classes.CEProductAdd = (function () {
    function CEProductAdd(root) {
      var _this;
      this.root = root;
      this.addProductComplete = bind(this.addProductComplete, this);
      this.addToCartListener = bind(this.addToCartListener, this);

      this.load = bind(this.load, this);
      _this = this;

      _this.add_button = _this.root.find(".add");
      _this.cart_form = _this.root.find(".product-form");

      _this.load();
    }

    CEProductAdd.prototype.load = function () {
      var _this;
      _this = this;

      return _this.addToCartListener();
    };

    CEProductAdd.prototype.addToCartListener = function () {
      var _this;
      _this = this;
      if (_this.cart_form.length > 0 && theme.settings.cart_type === "drawer") {
        return _this.cart_form.on("submit", function () {
          _this.cart_form.find(".error").remove();
          _this.add_button.attr("data-loading", "true");
          theme.partials.Cart.addItem($(this), function (success, error) {
            if (success) {
              return theme.partials.Cart.updateAllHtml(function () {
                return _this.addProductComplete();
              });
            } else {
              _this.cart_form.append(
                '<div class="featured-product--error error">' + error + "</div>"
              );
              return _this.add_button.attr("data-loading", "false");
            }
          });

          return false;
        });
      }
    };

    CEProductAdd.prototype.addProductComplete = function () {
      var _this;
      _this = this;
      _this.add_button.attr("data-loading", "false");
      return $('[data-off-canvas--open="right-sidebar"]')
        .first()
        .trigger("click");
    };

    return CEProductAdd;
  })();

  //theme.classes.CEProductAdd = (function(superClass) {
  //  extend(CEProductAdd, superClass);

  //  function CEProductAdd() {
  //    return CEProductAdd.__super__.constructor.apply(this, arguments);
  //  }

  //  return CEProductAdd;

  // })(theme.classes.CEProductAdd);

  // End Crabtree Bits

  theme.classes.Utils = (function (superClass) {
    extend(Utils, superClass);

    function Utils() {
      var _this;
      _this = this;
      Utils.__super__.constructor.apply(this, arguments);
      _this.sections = new theme.classes.Sections();
      _this.mqs = new theme.classes.FrameworkMediaQueries();
    }

    Utils.prototype.transparentMenuIsEnabled = function () {
      return PAGE.find("[data-header--menu-transparent=true]").length > 0;
    };

    Utils.prototype.loadJsClasses = function () {
      return $("[data-js-class]").each(function () {
        var js_class, partial_class;
        js_class = $(this).attr("data-js-class");
        if ($(this).attr("data-js-loaded") !== "true") {
          partial_class = theme.classes[js_class];
          if (typeof partial_class !== "undefined") {
            theme.partials[js_class] = new partial_class($(this));
            return $(this).attr("data-js-loaded", "true");
          }
        }
      });
    };

    Utils.prototype.addSymbol = function (icon_name) {
      return (
        '<svg class="fw--icon fw--icon--' +
        icon_name +
        '"> <use xlink:href="#fw--icon--' +
        icon_name +
        '" /> </svg>'
      );
    };

    Utils.prototype.isIE11 = function () {
      if (!!window.MSInputMethodContext && !!document.documentMode) {
        return true;
      }
      return false;
    };

    return Utils;
  })(theme.classes.FrameworkUtils);

  PAGE = null;

  DOC = null;

  WINDOW = null;

  HEADER = null;

  touchevents_exist = null;

  mq_small = null;

  mq_medium = null;

  mq_large = null;

  log = null;

  current_window = "";

  jQuery(function ($) {
    var isFirefox, page_content;
    PAGE = $("body");
    DOC = $(document);
    WINDOW = $(window);
    HEADER = $(".header--root");
    touchevents_exist = Modernizr.touchevents;
    mq_small = 768;
    mq_medium = 1280;
    mq_large = 1440;
    theme.utils = new theme.classes.Utils();
    if (general_external_links_enabled) {
      $('a[href^="http"]')
        .not('a[href^="' + shop_url + '"]')
        .attr("target", "_blank");
    }
    new Popup("popup");
    new Footer($(".footer"));
    new Accordion();
    new HorizontalTabs();
    new VerticalTabs();
    if (PAGE.hasClass("template-page")) {
      page_content = $(".page-content .rte-content");
      if (
        page_content.find(".left-side-column").length ||
        page_content.find(".right-side-column").length
      ) {
        if (
          page_content.find(".left-side-column").length &&
          page_content.find(".right-side-column").length
        ) {
          page_content.wrapInner(
            "<div class='main-column with-2-sidebars'></div>"
          );
          $(".left-side-column").addClass("with-2-sidebars");
          $(".right-side-column").addClass("with-2-sidebars");
        } else {
          page_content.wrapInner("<div class='main-column'></div>");
        }
        $(".left-side-column").prependTo(page_content);
        $(".right-side-column").appendTo(page_content);
      }
    }
    if (PAGE.hasClass("template-index")) {
      $(".slider").each(function () {
        return new FullscreenSlider($(this).find(".slides"));
      });
      $(".product-slider").each(function () {
        return new ProductSlider($(this));
      });
    }
    if (PAGE.hasClass("template-collection")) {
      new Collection();
    }
    if (PAGE.hasClass("template-password")) {
      $(document)
        .on("click", ".password--login-link", function () {
          $(this).css("visibility", "hidden");
          $(".password--main").hide();
          $(".password--login-form").css("visibility", "visible");
          $('.password--login-form input[type="password"]').focus();
          return false;
        })
        .on("click", ".password--login-form .cancel", function () {
          $(".password--login-link").css("visibility", "visible");
          $(".password--main").fadeIn();
          $(".password--login-form").css("visibility", "hidden");
          return false;
        });
      if ($(".password--login-form .errors").length) {
        $(".password--login-link").click();
      }
    }
    isFirefox = typeof InstallTrigger !== "undefined";
    if (isFirefox) {
      $("img").addClass("image-scale-hack");
    }
    if (touchevents_exist) {
      $("form.custom .hidden-field").removeClass("hidden-field");
    }
    current_window = "";
    new MediaQueries();
    $(".hide-until-js").show();
    DOC.on("shopify:section:load", function (e) {
      var section_wrapper;
      section_wrapper = $(e.target).closest(".shopify-section");
      section_wrapper.css("min-height", "2000px");
      if (section_wrapper.hasClass("section--slideshow")) {
        new FullscreenSlider($(e.target).find(".slides"));
      } else if (section_wrapper.hasClass("section--featured-collection")) {
        new ProductSlider($(e.target).find(".product-slider"));
      } else if (section_wrapper.hasClass("section--header")) {
        setTimeout(function () {
          if ($(".slider").length > 0) {
            return DOC.trigger("header.transparentMenuIsUpdated");
          }
        }, 0);
      } else if (section_wrapper.hasClass("section--products")) {
        DOC.trigger("set-option-selectors");
      }
      return section_wrapper.css("min-height", "auto");
    });
    DOC.on("shopify:section:unload", function (e) {
      var mask, section_wrapper;
      section_wrapper = $(e.target).closest(".shopify-section");
      if (section_wrapper.hasClass("section--featured-collection")) {
        return setTimeout(function () {
          if ($(".section--featured-collection").length === 0) {
            return WINDOW.off("resize.ProductSlider");
          }
        }, 0);
      } else if (section_wrapper.hasClass("section--products")) {
        mask = $(".popup-modal-mask");
        $(".size-chart").remove();
        if (mask.is(":visible")) {
          return mask.trigger("click");
        }
      } else if (section_wrapper.hasClass("section--slideshow")) {
        return setTimeout(function () {
          if ($(".slider").length === 0) {
            return DOC.off("header.transparentMenuIsUpdated");
          }
        }, 0);
      }
    });
    DOC.on("shopify:block:select", function (e) {
      var section_wrapper, slide_selected;
      section_wrapper = $(e.target).closest(".shopify-section");
      if (section_wrapper.hasClass("section--slideshow")) {
        slide_selected = section_wrapper.find(".slide").index(e.target);
        return $(e.target)
          .closest(".owl-carousel")
          .trigger("owl.jumpTo", slide_selected)
          .trigger("owl.stop");
      }
    });
    DOC.on("shopify:section:deselect", function (e) {
      var section_wrapper, should_autoplay, slider;
      section_wrapper = $(e.target).closest(".shopify-section");
      if (section_wrapper.hasClass("section--slideshow")) {
        should_autoplay = section_wrapper.find(".slider").data("autoplay");
        if (should_autoplay === true) {
          slider = section_wrapper.find(".slides");
          return slider.trigger(
            "owl.play",
            section_wrapper.find(".slider").data("rotateFrequency")
          );
        }
      }
    });
    theme.utils.loadJsClasses();
    return false;
  });
}.call(this));
