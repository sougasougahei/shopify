(() => {
  "use strict";

  const // development or production
    devBuild =
      (process.env.NODE_ENV || "development").trim().toLowerCase() ===
      "development",
    // directory locations
    dir = {
      src: "src/",
      build: "../assets/",
    },
    // modules
    gulp = require("gulp"),
    del = require("del"),
    noop = require("gulp-noop"),
    newer = require("gulp-newer"),
    size = require("gulp-size"),
    imagemin = require("gulp-imagemin"),
    sass = require("gulp-sass"),
    postcss = require("gulp-postcss"),
    svgSprite = require("gulp-svg-sprites"),
    concat = require("gulp-concat"),
    uglify = require("gulp-uglify"),
    sourcemaps = devBuild ? require("gulp-sourcemaps") : null;

  console.log("Gulp", devBuild ? "development" : "production", "build");

  //clean task

  function clean() {
    return del([dir.build]);
  }
  exports.clean = clean;
  exports.wipe = clean;

  // **************** images task **************** /;

  const imgConfig = {
    src: dir.src + "images/**/*",
    build: dir.build + "/",

    minOpts: {
      optimizationLevel: 5,
    },
  };

  function images() {
    return gulp
      .src(imgConfig.src)
      .pipe(newer(imgConfig.build))
      .pipe(imagemin(imgConfig.minOpts))
      .pipe(size({ showFiles: true }))
      .pipe(gulp.dest(imgConfig.build));
  }
  exports.images = images;

  // **************** Sprite task **************** /;
  const spriteConfig = {
    src: dir.src + "icons/*.svg",
    build: dir.build + "./../snippets/",
  };

  function sprite() {
    return gulp
      .src(spriteConfig.src)
      .pipe(
        svgSprite({
          mode: "symbols",
          preview: false,
          svg: {
            symbols: "ce-icons.svg.liquid",
          },
        })
      )
      .pipe(gulp.dest(spriteConfig.build));
  }
  exports.sprite = gulp.series(images, sprite);

  // **************** Vendor JS task **************** /;
  const vendorConfig = {
    src: dir.src + "js/vendor/**/*.js",
    build: dir.build + "/",
  };

  function vendor() {
    return gulp
      .src(vendorConfig.src)
      .pipe(uglify())
      .pipe(gulp.dest(vendorConfig.build));
  }

  exports.vendor = gulp.series(images, sprite, vendor);

  // **************** JS task **************** /;

  const jsConfig = {
    src: dir.src + "js/*.js",
    build: dir.build + "/",
  };

  function js() {
    return gulp
      .src(jsConfig.src)
      .pipe(uglify())
      .pipe(concat("production.min.js"))
      .pipe(gulp.dest(jsConfig.build));
  }
  exports.js = gulp.series(images, sprite, vendor, js);

  // **************** CSS task **************** /;

  const cssConfig = {
    src: dir.src + "scss/*.scss",
    watch: dir.src + "scss/**/*",
    build: dir.build + "/",
    sassOpts: {
      sourceMap: devBuild,
      outputStyle: "compressed", //compressed, nested, compact
      imagePath: "/images/",
      precision: 3,
      errLogToConsole: true,
    },

    postCSS: [
      require("postcss-assets")({
        loadPaths: ["images/"],
        basePath: dir.build,
      }),
      require("autoprefixer")({
        browsers: ["last 5 version"],
      }),
    ],
  };

  // remove unused selectors and minify production CSS
  // if (!devBuild) {
  //   cssConfig.postCSS.push(
  //     require("usedcss")({ html: ["index.html"] }),
  //     require("cssnano")
  //   );
  // }

  function css() {
    return gulp
      .src(cssConfig.src)
      .pipe(sourcemaps ? sourcemaps.init() : noop())
      .pipe(sass(cssConfig.sassOpts).on("error", sass.logError))
      .pipe(postcss(cssConfig.postCSS))
      .pipe(size({ showFiles: true }))
      .pipe(gulp.dest(cssConfig.build));
  }
  exports.css = gulp.series(images, sprite, vendor, js, css);

  // **************** watch task **************** /;

  function watch(done) {
    // image changes
    gulp.watch(imgConfig.src, images);

    // Sprite changes
    gulp.watch(spriteConfig.src, sprite);

    // Vendor changes
    gulp.watch(vendorConfig.src, vendor);

    // JS changes
    gulp.watch(jsConfig.src, js);

    // CSS changes
    gulp.watch(cssConfig.watch, css);

    done();
  }

  // **************** default task **************** /;

  exports.default = gulp.series(exports.css, watch);
})();
